#include "dataset.h"

dataset::dataset(const char *imgPath) {
  readImage(imgPath);
}

void dataset::readImage(const char *imgPath) {
  cimg_library::CImg<unsigned int> image(imgPath);
  width = image.width();
  height = image.height();
  Datasize = width * height;

  Datapoints = (float *)malloc(Datasize * 3 * sizeof(float));

  for (int i = 0; i < Datasize; i++) {
    int x = i % width;
    int y = i / width;
    
    Datapoints[R(i)] = image(x, y, 0);
    Datapoints[G(i)] = image(x, y, 1);
    Datapoints[B(i)] = image(x, y, 2);
  };
}

void dataset::writeImage(const char *imgPath) {
  cimg_library::CImg<float> image(width, height, 1, 3, 0);
  for (int i = 0; i < width * height; i++) {
    int x = i % width;
    int y = i / width;
    
    image(x, y, 0) = Datapoints[R(i)];
    image(x, y, 1) = Datapoints[G(i)];
    image(x, y, 2) = Datapoints[B(i)];
  }
  image.save_jpeg(imgPath, 100);
}