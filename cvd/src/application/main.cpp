
#include "dataset.h"
#include "grid.h"
#include "solver.h"
#include "utils.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <chrono>

using namespace std::chrono;

std::string imgpath = "testeimg1.jpg";
std::string outputpath = "svimg.jpg";
int num_imgs = 1;

solver *Init();
void Recolor(std::vector<solver *> solvers);
void svimg(solver *Solver, std::string outputpath);

void run(int type_run) {
  if (type_run == 0) {
    std::vector<solver *>solvers;
    solvers.push_back(Init());

    auto start = steady_clock::now();

    Recolor(solvers);

    #pragma omp taskwait

    auto end = steady_clock::now();

    svimg(solvers[0], outputpath);
    
    const duration<double> diff = end - start;
    printf("Execution time (%s): %fs\n", type_run == 0 ? "warm-up" : "real", diff.count());
  } else {

    std::vector<solver *> solvers;

    for (int i = 0; i < num_imgs; i++)
      solvers.push_back(Init());

    auto start = steady_clock::now();

    /*for (int i = 0; i < num_imgs; i++)
      Recolor(solvers[i]);*/
    Recolor(solvers);

    #pragma omp taskwait

    auto end = steady_clock::now();

    for (int i = 0; i < num_imgs; i++) {
      std::string output = std::to_string(i) + "_";
      output += outputpath;
      svimg(solvers[i], output);
    }

    const duration<double> diff = end - start;
    printf("Execution time (%s): %fs\n", type_run == 0 ? "warm-up" : "real", diff.count());
  }
}

// Programa Principal 
int main(int argc, char **argv) {
  bool warmup = true;

  if (argc > 1)
    imgpath = std::string(argv[1]);
  if (argc > 2) 
    num_imgs = std::stoi(argv[2]);
  if (argc > 3) {
    outputpath = std::string(argv[3]);
    warmup = false;
  }

  // warm up 
  if (warmup)
    run(0);
  sleep(1);
  run(1);

  return 0;
}

solver *Init() {
  grid *Grid = new grid(PROTANOPE, T_GRID);
  dataset *Dataset = new dataset(imgpath.c_str());
  solver *Solver = new solver(Grid, Dataset);
  return Solver;
}

void Recolor(std::vector<solver *> solvers) {
  int max_epochs = 7; //Solver->numEpochs;
  int curr_epoch = 0;

  for (auto Solver : solvers) Solver->startOMPC();
  for (auto Solver : solvers) Solver->convertLAB();
  while (curr_epoch < max_epochs) {
    for (auto Solver : solvers) Solver->resetSysMatrix();
    for (auto Solver : solvers) Solver->calcTaxons();
    for (auto Solver : solvers) Solver->constructSysMatrix();
    for (auto Solver : solvers) Solver->solveLS();
    // for (auto Solver : solvers) Solver->CurrentEpoch++;
    curr_epoch++;
  }
  for (auto Solver : solvers) Solver->centerWhite();
  for (auto Solver : solvers) Solver->projectPoints();
  for (auto Solver : solvers) Solver->convertRGB();
  for (auto Solver : solvers) Solver->finishOMPC();
}

void svimg(solver *Solver, std::string outputpath) {
  Solver->writeImage(outputpath.c_str());
  printf("image saved as %s\n", outputpath.c_str());
}