
#include "solver.h"

#include <omp.h>

#include "kernels.h"

solver::solver(grid *A, dataset *B) {
  Grid = A;
  Dataset = B;

  // Size informations
  numEpochs = 7;
  zeroNode = -1;
  Emodule = 0.1;
  Rmodule = 50;
  CurrentEpoch = 0;
  GridDimension = 2;
  numNodes = A->graph_size;
  numPixels = Dataset->Datasize;

  OriginalMap = (float *)malloc(numNodes * 2 * sizeof(float));
  // Lab colors, each Pixel has 3 values [L, a, b]
  Datapoints = (float *)malloc(numPixels * 3 * sizeof(float));
  // Final Solution, each Pixel has 3 values [L, a, b]
  Projection = (float *)malloc(numPixels * 3 * sizeof(float));

  // System Matrix
  Taxons = (int *)malloc(numPixels * sizeof(int));
  ResultVector = (float *)malloc(numNodes * 2 * sizeof(float));
  Solution = (float *)malloc(numNodes * 2 * sizeof(float));
  SysMatrix = (float *)malloc(numNodes * numNodes * sizeof(float));

  // Define the rates of Elastic Map for each epoch
  EpochM = (float *)malloc(numEpochs * sizeof(float));
  float epo = 0.05;
  for (int i = 0; i < numEpochs; i++) {
    EpochM[i] = epo;
    epo /= (3 - i * 0.04);
  }
}

// ====== PREPARATION
void solver::convertLAB() {
  // Task Preparation
  float *rgb_points = Dataset->Datapoints;
  float *datapoints = Datapoints;
  int gNumPixels = numPixels;

  // Task Creation
#pragma omp target depend(in: rgb_points[0])                        \
                   depend(inout: datapoints[0])                     \
                   nowait
  convert_to_lab(rgb_points, datapoints, gNumPixels);
}

// ====== MAIN LOOP
void solver::calcTaxons() {
  // Task Preparation
  float *datapoints = Datapoints;
  float *location = Grid->Location;
  int *taxons = Taxons;
  int gNumPixels = numPixels, gNumNodes = numNodes;

  // Task Creation
#pragma omp target depend(in: datapoints[0])                     \
                   depend(in: location[0])                       \
                   depend(inout: taxons[0])                         \
                   nowait
  calculate_taxons(datapoints, location, taxons, gNumPixels);
}

void solver::resetSysMatrix() {
  // Task Preparation
  float *sysmatrix = SysMatrix;
  int gNumNodes = numNodes;

  // Task Creation
#pragma omp target depend(inout: sysmatrix[0])                      \
                   nowait
  prepare_matrix(sysmatrix, gNumNodes);

  Emodule = EpochM[CurrentEpoch] *
            pow(Grid->numEdges, (2 - GridDimension) / GridDimension) * 10;
  Rmodule = EpochM[CurrentEpoch] *
            pow(Grid->numRibs, (2 - GridDimension) / GridDimension) * 100;
}

void solver::constructSysMatrix() {
  // Task Preparation
  float *resultvector = ResultVector;
  float *sysmatrix = SysMatrix;
  int *taxons = Taxons;
  float *datapoints = Datapoints;
  int *ribs = Grid->Ribs;
  int *edges = Grid->Edges;
  int gNumRibs = Grid->numRibs;
  int gNumEdges = Grid->numEdges;
  int gNumNodes = numNodes;
  int gNumPixels = numPixels;
  float gRModule = Rmodule;
  float gEModule = Emodule;

  // Task Creation
#pragma omp target depend(inout: resultvector[0], sysmatrix[0])    \
                   depend(in: datapoints[0])   \
                   depend(in: ribs[0], edges[0], taxons[0])      \
                   nowait
  construct_matrix(resultvector, sysmatrix, taxons, datapoints, ribs, edges,
                   gNumRibs, gNumEdges, gNumPixels, gNumNodes, gRModule,
                   gEModule);
}

void solver::solveLS() {  
  // Task Preparation
  float *location = Grid->Location;
  float *resultvector = ResultVector;
  float *sysmatrix = SysMatrix;
  int gNumNodes = numNodes;

  // Task Creation
#pragma omp target depend(in: resultvector[0], SysMatrix[0])     \
                   depend(inout: location[0])                       \
                   nowait
  solver_linear_system(resultvector, sysmatrix, location, gNumNodes);
}

// ====== ENDING
void solver::centerWhite() {
  // Task Preparation
  float *datapoints = Datapoints;
  float *originalmap = OriginalMap;
  int *taxons = Taxons;
  int gNumNodes = numNodes;
  int gNumPixels = numPixels;
  float a_0 = Grid->A[0];
  float c_0 = Grid->C[0];
  float mi_AB = Grid->miAB;
  float mi_BC = Grid->miBC;

  // Task Creation
#pragma omp target depend(inout: originalmap[0])          \
                   depend(in: taxons[0], datapoints[0])   \
                   nowait
  center_white_node(datapoints, taxons, originalmap, a_0, c_0, mi_AB, mi_BC,
                    gNumNodes, gNumPixels);
}

void solver::projectPoints() {
  // Task Preparation
  float *datapoints = Datapoints;
  float *projection = Projection;
  int *taxons = Taxons;
#if CENTERWHITE
  float *cvd_map = OriginalMap;
#else
  float *cvd_map = Grid->CVDPosition;
#endif
  int num_pixels = numPixels;

  // Task Creation
#pragma omp target depend(inout: projection[0])         \
                   depend(in: taxons[0], cvd_map[0], datapoints[0])   \
                   nowait
  project_on_plane(datapoints, projection, taxons, cvd_map, num_pixels);
}

void solver::convertRGB() {
  // Task Preparation
  float *rgb_points = Dataset->Datapoints;
  float *projection = Projection;
  int gNumPixels = numPixels;

  // Task Creation
#pragma omp target depend(inout: rgb_points[0])   \
                   depend(in: projection[0])      \
                   nowait
  convert_from_lab(projection, rgb_points, gNumPixels);
}

// ====== EXTRA FUNCTIONS
void solver::startOMPC() {
  int gNumNodes = numNodes;
  int gNumPixels = numPixels;  
  int gNumRibs = Grid->numRibs;
  int gNumEdges = Grid->numEdges;
  int gGraphSize = Grid->graph_size * 2;

  // Image Pixel Related Buffers
  float *rgb_points = Dataset->Datapoints;
#pragma omp target enter data map(to: rgb_points[: gNumPixels * 3]) depend(out: *rgb_points) nowait
  float *datapoints = Datapoints;
#pragma omp target enter data map(alloc: datapoints[: gNumPixels * 3]) depend(out: *datapoints) nowait
  float *projection = Projection;
#pragma omp target enter data map(alloc: projection[: gNumPixels * 3]) depend(out: *projection) nowait

  // Grid Map Related Buffers
  float *location = Grid->Location;
#pragma omp target enter data map(to: location[: gGraphSize]) depend(out: *location) nowait
  float *cvd_position = Grid->CVDPosition;
#pragma omp target enter data map(to: cvd_position[: gNumNodes * 2]) depend(out: *cvd_position) nowait
  float *originalmap = OriginalMap;
#pragma omp target enter data map(to: originalmap[: gNumNodes * 2]) depend(out: *originalmap) nowait
  int *ribs = Grid->Ribs;
#pragma omp target enter data map(to: ribs[: gNumRibs * 3]) depend(out: *ribs) nowait
  int *edges = Grid->Edges;
#pragma omp target enter data map(to: edges[: gNumEdges * 2]) depend(out: *edges) nowait

  // Solver Related Buffers
  float *sysmatrix = SysMatrix;
#pragma omp target enter data map(alloc: sysmatrix[: gNumNodes * gNumNodes]) depend(out: *sysmatrix) nowait
  float *resultvector = ResultVector;
#pragma omp target enter data map(alloc: resultvector[: gNumNodes * 2]) depend(out: *resultvector) nowait
  int *taxons = Taxons;
#pragma omp target enter data map(alloc: taxons[: gNumPixels]) depend(out: *taxons) nowait
}

void solver::finishOMPC() {
  int gNumNodes = numNodes;
  int gNumPixels = numPixels;  
  int gNumRibs = Grid->numRibs;
  int gNumEdges = Grid->numEdges;
  int gGraphSize = Grid->graph_size * 2;

  // Image Pixel Related Buffers
  float *rgb_points = Dataset->Datapoints;
#pragma omp target exit data map(from: rgb_points[: gNumPixels * 3]) depend(inout: *rgb_points) nowait
  float *datapoints = Datapoints;
#pragma omp target exit data map(delete: datapoints[: gNumPixels * 3]) depend(inout: *datapoints) nowait
  float *projection = Projection;
#pragma omp target exit data map(delete: projection[: gNumPixels * 3]) depend(inout: *projection) nowait

  // Grid Map Related Buffers
  float *location = Grid->Location;
#pragma omp target exit data map(delete: location[: gGraphSize]) depend(inout: *location) nowait
  float *cvd_position = Grid->CVDPosition;
#pragma omp target exit data map(delete: cvd_position[: gNumNodes * 2]) depend(inout: *cvd_position) nowait
  float *originalmap = OriginalMap;
#pragma omp target exit data map(delete: originalmap[: gNumNodes * 2]) depend(inout: *originalmap) nowait
  int *ribs = Grid->Ribs;
#pragma omp target exit data map(delete: ribs[: gNumRibs * 3]) depend(inout: *ribs) nowait
  int *edges = Grid->Edges;
#pragma omp target exit data map(delete: edges[: gNumEdges * 2]) depend(inout: *edges) nowait

  // Solver Related Buffers
  float *sysmatrix = SysMatrix;
#pragma omp target exit data map(delete: sysmatrix[: gNumNodes * gNumNodes]) depend(inout: *sysmatrix) nowait
  float *resultvector = ResultVector;
#pragma omp target exit data map(delete: resultvector[: gNumNodes * 2]) depend(inout: *resultvector) nowait
  int *taxons = Taxons;
#pragma omp target exit data map(delete: taxons[: gNumPixels]) depend(inout: *taxons) nowait
}

void solver::writeImage(const char *imgPath) {
  Dataset->writeImage(imgPath);
}