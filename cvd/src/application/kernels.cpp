#include "kernels.h"

using namespace std::chrono;

/* Declaration of OpenMP Targets for Kernels */
#pragma omp declare target(convert_to_lab)
#pragma omp declare target(calculate_taxons)
#pragma omp declare target(prepare_matrix)
#pragma omp declare target(construct_matrix)
#pragma omp declare target(solver_linear_system)
#pragma omp declare target(center_white_node)
#pragma omp declare target(project_on_plane)
#pragma omp declare target(convert_from_lab)

/* Implementation of the CPU Functions */
/* Preparation */
void convert_to_lab(float *rgb, float *lab, int num_pixels) {
  //printf("---------------------- %20s ---------------------- \n", "Convert to LAB");
  auto start = steady_clock::now();
  for (int i = 0; i < num_pixels; i++) {
    float *lab_color = getLabColor(rgb[R(i)], rgb[G(i)], rgb[B(i)]);

    // Fill data points [0: L; 1: a; 2: b]
    lab[L(i)] = lab_color[0];
    lab[a(i)] = lab_color[1];
    lab[b(i)] = lab_color[2];
  };
  auto end = steady_clock::now();
  const duration<double> diff = end - start;
  printf("Execution time (Convert to LAB - CPU): %fs\n", diff.count());
}

/* Main loop - 7 Iterations*/
void calculate_taxons(float *datapoints, float *location, int *taxons,
                      int num_pixels) {
  // printf("---------------------- %20s ---------------------- \n", "Calculate Taxons");
  auto start = steady_clock::now();
  for (int i = 0; i < num_pixels; i++) {
    float min_dist = (float)MAX_VALUE;
    int min_node_ref;
    // For each pixel, which ElMap graph node is closest one?
    for (int j = 0; j < T_GRID; j++) {
      float x1 = datapoints[a(i)];
      float y1 = datapoints[b(i)];
      float x2 = location[pX(j)];
      float y2 = location[pY(j)];
      float local_dist = ((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2));
      if (local_dist < min_dist) {
        min_dist = local_dist;
        min_node_ref = j;
      }
    }
    taxons[i] = min_node_ref;
  }
  auto end = steady_clock::now();
  const duration<double> diff = end - start;
  printf("Execution time (Calculate Taxons): %fs\n", diff.count());
}

void prepare_matrix(float *sys_matrix, int num_nodes) {
  // printf("---------------------- %20s ---------------------- \n", "Reset Matrix");
  auto start = steady_clock::now();
  for (int i = 0; i < num_nodes; i++)
    for (int j = 0; j < num_nodes; j++)
      sys_matrix[posN(i, j, num_nodes)] = 0;
  auto end = steady_clock::now();
  const duration<double> diff = end - start;
  printf("Execution time (Reset Matrix): %fs\n", diff.count());
}

void construct_matrix(float *result_vector, float *sys_matrix, int *taxons,
                      float *datapoints, int *ribs, int *edges, int num_ribs,
                      int num_edges, int num_pixels, int num_nodes,
                      float r_module, float e_module) {
  // printf("---------------------- %20s ---------------------- \n", "Construct Matrix");
  auto start = steady_clock::now();
  int *TaxonSizes = (int *)malloc(num_nodes * sizeof(int));

  // Calculate the centroids of each taxon
  for (int i = 0; i < num_nodes; i++) {
    result_vector[pos2(i, 0)] = 0;
    result_vector[pos2(i, 1)] = 0;
    TaxonSizes[i] = 0;
  }
  for (int i = 0; i < num_pixels; i++) {
    result_vector[pos2(taxons[i], 0)] += datapoints[a(i)];
    result_vector[pos2(taxons[i], 1)] += datapoints[b(i)];
    TaxonSizes[taxons[i]] += 1;
  }
  for (int i = 0; i < num_nodes; i++) {
    result_vector[pos2(i, 0)] /= (1.0 * num_pixels);
    result_vector[pos2(i, 1)] /= (1.0 * num_pixels);
  }

  // Influence of the ribs on the Elastic Map
  for (int i = 0; i < num_ribs; i++) {
    int N0 = ribs[i * 3 + 0];
    int N1 = ribs[i * 3 + 1];
    int N2 = ribs[i * 3 + 2];

    sys_matrix[posN(N0, N0, num_nodes)] += r_module * 4.0;
    sys_matrix[posN(N0, N1, num_nodes)] -= r_module * 2.0;
    sys_matrix[posN(N0, N2, num_nodes)] -= r_module * 2.0;
    sys_matrix[posN(N1, N0, num_nodes)] -= r_module * 2.0;
    sys_matrix[posN(N2, N0, num_nodes)] -= r_module * 2.0;
    sys_matrix[posN(N1, N1, num_nodes)] += r_module;
    sys_matrix[posN(N1, N2, num_nodes)] += r_module;
    sys_matrix[posN(N2, N1, num_nodes)] += r_module;
    sys_matrix[posN(N2, N2, num_nodes)] += r_module;
  }

  // Influence of the Edges on the Elastic Map
  for (int i = 0; i < num_edges; i++) {
    int N0 = edges[i * 2 + 0];
    int N1 = edges[i * 2 + 1];

    sys_matrix[posN(N0, N0, num_nodes)] += e_module;
    sys_matrix[posN(N1, N1, num_nodes)] += e_module;
    sys_matrix[posN(N0, N1, num_nodes)] -= e_module;
    sys_matrix[posN(N1, N0, num_nodes)] -= e_module;
  }

  // Influence of the Taxons on the Elastic Map
  for (int i = 0; i < num_nodes; i++) {
    sys_matrix[posN(i, i, num_nodes)] += TaxonSizes[i] / (1.0 * num_pixels);
  }
  free(TaxonSizes);
  auto end = steady_clock::now();
  const duration<double> diff = end - start;
  printf("Execution time (Construct Matrix): %fs\n", diff.count());
}

void solver_linear_system(float *result_vector, float *sys_matrix,
                          float *location, int num_nodes) {
  // printf("---------------------- %20s ---------------------- \n", "Solver Linear Sys.");
  auto start = steady_clock::now();
  float *solution = (float *)malloc(num_nodes * 2 * sizeof(float));
  // escalonador
  for (int k = 0; k < num_nodes; k++) {
    for (int j = k + 1; j < num_nodes; j++) {
      float multi =
          sys_matrix[posN(j, k, num_nodes)] / sys_matrix[posN(k, k, num_nodes)];
      for (int i = k; i < num_nodes; i++)
        sys_matrix[posN(j, i, num_nodes)] -=
            sys_matrix[posN(k, i, num_nodes)] * multi;

      result_vector[pos2(j, 0)] -= result_vector[pos2(k, 0)] * multi;
      result_vector[pos2(j, 1)] -= result_vector[pos2(k, 1)] * multi;
    }
  }
  // resolve
  for (int k = num_nodes - 1; k >= 0; k--) {
    double sum_x = 0;
    double sum_y = 0;
    for (int j = k + 1; j < num_nodes; j++) {
      sum_x += sys_matrix[posN(k, j, num_nodes)] * solution[pos2(j, 0)];
      sum_y += sys_matrix[posN(k, j, num_nodes)] * solution[pos2(j, 1)];
    }
    solution[pos2(k, 0)] =
        (result_vector[pos2(k, 0)] - sum_x) / sys_matrix[posN(k, k, num_nodes)];
    solution[pos2(k, 1)] =
        (result_vector[pos2(k, 1)] - sum_y) / sys_matrix[posN(k, k, num_nodes)];
  }

  for (int i = 0; i < num_nodes; i++) {
    location[pX(i)] = solution[pos2(i, 0)];
    location[pY(i)] = solution[pos2(i, 1)];
  }
  auto end = steady_clock::now();
  const duration<double> diff = end - start;
  printf("Execution time (Solver Linear Sys.): %fs\n", diff.count());
}

void center_white_node(float *datapoints, int *taxons, float *original_map,
                       float a_0, float c_0, float mi_AB, float mi_BC,
                       int num_nodes, int num_pixels) {
  // printf("---------------------- %20s ---------------------- \n", "Center White Node");
  auto start = steady_clock::now();
  float min_distance = (float)MAX_VALUE;
  int zero_node = num_nodes / 2;
  for (int i = 0; i < num_pixels; i++) {
    float x1 = datapoints[a(i)];
    float y1 = datapoints[b(i)];
    float x2 = 0;
    float y2 = 0;
    float local_zero_dist = ((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2));
    if (local_zero_dist < min_distance) {
      zero_node = taxons[i];
      min_distance = local_zero_dist;
    }
  }

  // printf("Origin Node: %d , i\;n", zero_node);

  int size_positive = num_nodes - zero_node;
  int size_negative = zero_node;
  float x_step =
      fabs((size_positive > size_negative) ? (a_0 / (1.0 * size_positive))
                                           : (c_0 / (1.0 * size_negative)));

  float x_start = 0;
  for (int i = zero_node; i < num_nodes; i++) {
    original_map[pos2(i, 0)] = x_start;
    original_map[pos2(i, 1)] = mi_AB * x_start;
    x_start += x_step;
  }

  x_start = 0 - x_step;
  for (int i = zero_node - 1; i >= 0; i--) {
    original_map[pos2(i, 0)] = x_start;
    original_map[pos2(i, 1)] = mi_BC * x_start;
    x_start -= x_step;
  }
  auto end = steady_clock::now();
  const duration<double> diff = end - start;
  printf("Execution time (Center White Node): %fs\n", diff.count());
}

void project_on_plane(float *datapoints, float *projection, int *taxons,
                      float *cvd_map, int num_pixels) {
  // printf("---------------------- %20s ---------------------- \n", "Project on Plane");
  auto start = steady_clock::now();
  for (int i = 0; i < num_pixels; i++) {
    int point = i;
    int node = taxons[i];

    projection[L(point)] = datapoints[L(point)];
    projection[a(point)] = cvd_map[pX(node)];
    projection[b(point)] = cvd_map[pY(node)];
  }
  auto end = steady_clock::now();
  const duration<double> diff = end - start;
  printf("Execution time (Project on Plane): %fs\n", diff.count());
}

void convert_from_lab(float *lab, float *rgb, int num_pixels) {
  // printf("---------------------- %20s ---------------------- \n", "Convert to RGB");
  auto start = steady_clock::now();
  for (int i = 0; i < num_pixels; i++) {
    float *rgb_color = getRGBColor(lab[L(i)], lab[a(i)], lab[b(i)]);
    for (int j = 0; j < 3; j++) {
      rgb_color[j] = (rgb_color[j] > 1.0) ? 1.0 : rgb_color[j];
      rgb_color[j] = (rgb_color[j] < 0.0) ? 0.0 : rgb_color[j];
    }
    rgb[L(i)] = rgb_color[0] * 255.0;
    rgb[a(i)] = rgb_color[1] * 255.0;
    rgb[b(i)] = rgb_color[2] * 255.0;
  };
  auto end = steady_clock::now();
  const duration<double> diff = end - start;
  printf("Execution time (Convert to RGB): %fs\n", diff.count());
}