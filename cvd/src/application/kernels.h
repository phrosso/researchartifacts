#include <omp.h>

#include <stdio.h>
#include <chrono>

#include "color_space.h"
#include "dataset.h"
#include "grid.h"
#include "solver.h"

// ====== Variants
/* FPGA Variants Prototypes */
void calctaxons_hw(float *datapoints, float *location, int *taxons,
                   int num_pixels);

// ====== Project Pipeline Start (assuming we start with rgb pixel values)
/* Preparation */
#pragma omp declare variant(convert_to_lab_gpu) match(user = {condition(ENABLE_CUDA)})
void convert_to_lab(float *rgb, float *lab, int num_pixels);

/* Loop for 7 Iterations */
#pragma omp declare variant(calctaxons_hw) match(device = {arch(alveo)})
void calculate_taxons(float *datapoints, float *location, int *taxons,
                      int num_pixels);
void prepare_matrix(float *sys_matrix, int num_nodes);
void construct_matrix(float *result_vector, float *sys_matrix, int *taxons,
                      float *datapoints, int *ribs, int *edges, int num_ribs,
                      int num_edges, int num_pixels, int num_nodes,
                      float r_module, float e_module);
void solver_linear_system(float *result_vector, float *sys_matrix,
                          float *location, int num_nodes);

/* Ending */
void center_white_node(float *datapoints, int *taxons, float *original_map,
                       float a_0, float c_0, float mi_AB, float mi_BC,
                       int num_nodes, int num_pixels);
void project_on_plane(float *datapoints, float *projection, int *taxons,
                      float *cvd_map, int num_pixels);

#pragma omp declare variant(convert_from_lab_gpu) match(user = {condition(ENABLE_CUDA)})
void convert_from_lab(float *lab, float *rgb, int num_pixels);


// ====== Project Pipeline End (assuming we finish with rgb pixel values)