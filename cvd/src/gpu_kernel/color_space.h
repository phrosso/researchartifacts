#ifndef COLORSPACE_H
#define COLORSPACE_H

void convert_to_lab_gpu(float *rgb, float *lab, int numPixels);
void convert_from_lab_gpu(float *lab, float *rgb, int numPixels);

#endif /* COLORSPACE_H */
