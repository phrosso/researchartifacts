
// CUDA headers
#include <chrono>
using namespace std::chrono;
#include <stdio.h>

#ifdef __NVCC__
#include <cuda.h>
#include <cuda_runtime.h>

__device__ void GPULabColor(unsigned int sR, unsigned int sG, unsigned int sB, float Lab[3])
{
  float X_ref = 94.811; // D65 Observer - Daylight, sRGB, Adobe-RGB
  float Y_ref = 100.000;
  float Z_ref = 107.304;

  float r = sR / 255.0, g = sG / 255.0, b = sB / 255.0, x, y, z, fx, fy, fz;

  // RGB to XYZ
  x = (r * 0.4124 + g * 0.3576 + b * 0.1805) / (X_ref / 100.0);
  y = (r * 0.2126 + g * 0.7152 + b * 0.0722) / (Y_ref / 100.0);
  z = (r * 0.0193 + g * 0.1192 + b * 0.9505) / (Z_ref / 100.0);

  // XYZ to Lab
  fx = (x > 0.008856) ? __powf(x, 0.3333333333) : (7.787 * x) + 16.0 / 116.0;
  fy = (y > 0.008856) ? __powf(y, 0.3333333333) : (7.787 * y) + 16.0 / 116.0;
  fz = (z > 0.008856) ? __powf(z, 0.3333333333) : (7.787 * z) + 16.0 / 116.0;

  Lab[0] = (116 * fy) - 16;
  Lab[1] = 500 * (fx - fy);
  Lab[2] = 200 * (fy - fz);
}

__global__ void GPUgetLabPoints(float *rgb, float *lab, int size)
{
  int i = threadIdx.x + (blockDim.x * blockIdx.x);
  if (i < size)
  {
    float lab_color[3];

    GPULabColor(rgb[i * 3 + 0], rgb[i * 3 + 1], rgb[i * 3 + 2], lab_color);

    // Fill data points [0: L; 1: a; 2: b]
    lab[i * 3 + 0] = lab_color[0];
    lab[i * 3 + 1] = lab_color[1];
    lab[i * 3 + 2] = lab_color[2];
  }
}

__device__ void GPURGBColor(float L, float a, float b, float RGB[3]) {
  float X_ref = 94.811 / 100.0; // D65 Observer - Daylight, sRGB, Adobe-RGB
  float Y_ref = 100.000 / 100.0;
  float Z_ref = 107.304 / 100.0;

  float fy = (L + 16.0) / 116.0, fx = (a / 500.0) + fy, fz = fy - (b / 200.0),
        xr, yr, zr, X, Y, Z;

  xr = (fx * fx * fx > 0.008856) ? fx * fx * fx : ((116.0 * fx) - 16.0) / 903.3;
  yr = (L > (903.3 * 0.008856)) ? fy * fy * fy : L / 903.3;
  zr = (fz * fz * fz > 0.008856) ? fz * fz * fz : ((116.0 * fz) - 16.0) / 903.3;

  X = xr * X_ref;
  Y = yr * Y_ref;
  Z = zr * Z_ref;

  RGB[0] = X * 3.2406 + Y * -1.5372 + Z * -0.4986;
  RGB[1] = X * -0.9689 + Y * 1.8758 + Z * 0.0415;
  RGB[2] = X * 0.0557 + Y * -0.2040 + Z * 1.0570;
}

__global__ void GPUgetRGBPoints(float *lab, float *rgb, int size)
{
  int i = threadIdx.x + (blockDim.x * blockIdx.x);
  if (i < size)
  {
    float rgb_color[3];
    GPURGBColor(lab[i * 3 + 0], lab[i * 3 + 1], lab[i * 3 + 2], rgb_color);

    for (int j = 0; j < 3; j++)
    {
      rgb_color[j] = (rgb_color[j] > 1.0) ? 1.0 : rgb_color[j];
      rgb_color[j] = (rgb_color[j] < 0.0) ? 0.0 : rgb_color[j];
    }
    rgb[i * 3 + 0] = rgb_color[0] * 255.0;
    rgb[i * 3 + 1] = rgb_color[1] * 255.0;
    rgb[i * 3 + 2] = rgb_color[2] * 255.0;
  }
}

void convert_to_lab_gpu(float *rgb, float *lab, int numPixels)
{
  auto start = steady_clock::now();
  /* Prepare buffers */
  float *d_rgb, *d_lab;

  cudaMalloc((void **)&d_lab, numPixels * 3 * sizeof(float));
  cudaMalloc((void **)&d_rgb, numPixels * 3 * sizeof(float)); 

  // Graph and Datapoints needs to be copied to GPU
  auto stage1 = steady_clock::now();
  cudaMemcpy(d_rgb, rgb, numPixels * 3 * sizeof(float), cudaMemcpyHostToDevice);
  auto stage2 = steady_clock::now();

  int div = (numPixels / 1024) + 1;
  GPUgetLabPoints<<<div, 1024>>>(d_rgb, d_lab, numPixels);
  cudaDeviceSynchronize();

  // Taxons need to be copied back
  auto stage3 = steady_clock::now();
  cudaMemcpy(lab, d_lab, numPixels * 3 * sizeof(float), cudaMemcpyDeviceToHost);
  auto stage4 = steady_clock::now();

  auto end = steady_clock::now();
  const duration<double> diff = end - start;
  const duration<double> stag1 = stage2 - stage1;
  const duration<double> stag2 = stage4 - stage3;
  printf("Execution time (Convert to LAB - GPU): %fs\n", diff.count());
  printf("Staging (Convert to LAB - GPU): %fs\n", stag1.count() + stag2.count());
}

void convert_from_lab_gpu(float *lab, float *rgb, int numPixels)
{
  auto start = steady_clock::now();
  /* Prepare buffers */
  float *d_rgb, *d_lab;

  cudaMalloc((void **)&d_rgb, numPixels * 3 * sizeof(float));
  cudaMalloc((void **)&d_lab, numPixels * 3 * sizeof(float));

  // Graph and Datapoints needs to be copied to GPU
  auto stage1 = steady_clock::now();
  cudaMemcpy(d_lab, lab, numPixels * 3 * sizeof(float), cudaMemcpyHostToDevice);
  auto stage2 = steady_clock::now();


  int div = (numPixels / 1024) + 1;
  GPUgetRGBPoints<<<div, 1024>>>(d_lab, d_rgb, numPixels);
  cudaDeviceSynchronize();

  // Taxons need to be copied back
  auto stage3 = steady_clock::now();
  cudaMemcpy(rgb, d_rgb, numPixels * 3 * sizeof(float), cudaMemcpyDeviceToHost);
  auto stage4 = steady_clock::now();


  auto end = steady_clock::now();
  const duration<double> diff = end - start;
  const duration<double> stag1 = stage2 - stage1;
  const duration<double> stag2 = stage4 - stage3;
  printf("Execution time (Convert to RGB - GPU): %fs\n", diff.count());
  printf("Staging (Convert to RGB - GPU): %fs\n", stag1.count() + stag2.count());
}

#else

void convert_to_lab_gpu(float *rgb, float *lab, int numPixels)
{
}

void convert_from_lab_gpu(float *lab, float *rgb, int numPixels)
{
}

#endif