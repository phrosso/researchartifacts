#include "kernel.h"

float dist(float x1, float x2, float y1, float y2) {
    return ((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2));
}


int MinRef(float *local_loc, float x1, float y1) {
	float MinDist = (float) MAX_VALUE;
	int MinNodeRef = 0;
	
	minref_internal_loop:
	for (int j = 0; j < T_GRID; j++) {
#pragma HLS pipeline II=2
        float x2 = local_loc[j * 2];
        float y2 = local_loc[j * 2 + 1];
        float localDist = dist(x1, x2, y1, y2);
        
        if (localDist < MinDist) {
            MinDist = localDist;
            MinNodeRef = j;
        }     
	}  
	
	return MinNodeRef;
}

void copy_location(float *local_loc, float *location) {    
    copy_loop:
    for (int i = 0; i < 200; i++) {
        local_loc[i] = location[i];
    }
}

void calctaxons_hw(float *datapoints, float *location, int *out, int elements) {
#pragma HLS INTERFACE m_axi depth = 192 port = datapoints bundle = gmem0 max_read_burst_length = 64 max_write_burst_length = 64
#pragma HLS INTERFACE m_axi depth = 200 port = location   bundle = gmem1 max_read_burst_length = 64 max_write_burst_length = 64
#pragma HLS INTERFACE m_axi depth = 64  port = out        bundle = gmem0 max_read_burst_length = 64 max_write_burst_length = 64
#pragma HLS INTERFACE s_axilite port = elements
#pragma HLS INTERFACE s_axilite port = return

    float local_loc[200];
    copy_location(local_loc, location);

    main_loop:
	for (int i = 0; i < elements; i++) {
#pragma HLS pipeline II=2
        float x1 = datapoints[i * 3 + 1];
        float y1 = datapoints[i * 3 + 2];
        
        out[i] = MinRef(local_loc, x1, y1);
	}
}

























