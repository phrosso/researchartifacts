#! /bin/bash

dir=$1
rm $dir/FinalResults.txt
touch $dir/FinalResults.txt

# Parse 100x100 results
#echo "100x100 Average: " > $dir/FinalResults.txt
cat $dir/100.txt | awk '/real\): /{sub(/.*real\): /, ""); sub(/s/, ""); print}' |  awk '{ SUM += $1; n++} END { printf "%.6f\n", SUM / n; }' >> $dir/FinalResults.txt

# Parse 200x200 results
#echo "200x200 Average: " >> $dir/FinalResults.txt
cat $dir/200.txt | awk '/real\): /{sub(/.*real\): /, ""); sub(/s/, ""); print}' |  awk '{ SUM += $1; n++} END { printf "%.6f\n", SUM / n; }' >> $dir/FinalResults.txt

# Parse 400x400 results
#echo "400x400 Average: " >> $dir/FinalResults.txt
cat $dir/400.txt | awk '/real\): /{sub(/.*real\): /, ""); sub(/s/, ""); print}' |  awk '{ SUM += $1; n++} END { printf "%.6f\n", SUM / n; }' >> $dir/FinalResults.txt

# Parse 800x800 results
#echo "800x800 Average: " >> $dir/FinalResults.txt
cat $dir/800.txt | awk '/real\): /{sub(/.*real\): /, ""); sub(/s/, ""); print}' |  awk '{ SUM += $1; n++} END { printf "%.6f\n", SUM / n; }' >> $dir/FinalResults.txt

# Parse 1600x1600 results
#echo "1600x1600 Average: " >> $dir/FinalResults.txt
cat $dir/1600.txt | awk '/real\): /{sub(/.*real\): /, ""); sub(/s/, ""); print}' |  awk '{ SUM += $1; n++} END { printf "%.6f\n", SUM / n; }' >> $dir/FinalResults.txt