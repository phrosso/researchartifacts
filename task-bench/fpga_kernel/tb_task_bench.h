
#include <cassert>
#include <cstring>
#include <iostream>
#include <map>
#include <set>
#include <string>

#include "task_bench.h"

#define MAX_NUM_ARGS 10

struct task_args_t {
  int x;
  int y;
};

struct tile_t {
  float dep;
  char *output_buff;
};

struct matrix_t {
  tile_t *data;
  int M;
  int N;
};

std::string to_string(DependenceType dep) {
  switch (dep) {
  case DependenceType::TRIVIAL:
    return std::string("trivial");
  case DependenceType::NO_COMM:
    return std::string("no_comm");
  case DependenceType::STENCIL_1D:
    return std::string("stencil_1d");
  case DependenceType::STENCIL_1D_PERIODIC:
    return std::string("stencil_1d_periodic");
  case DependenceType::DOM:
    return std::string("dom");
  case DependenceType::TREE:
    return std::string("tree");
  case DependenceType::FFT:
    return std::string("fft");
  case DependenceType::ALL_TO_ALL:
    return std::string("all_to_all");
  case DependenceType::NEAREST:
    return std::string("nearest");
  case DependenceType::SPREAD:
    return std::string("spread");
  case DependenceType::RANDOM_NEAREST:
    return std::string("random_nearest");
  case DependenceType::RANDOM_SPREAD:
    return std::string("random_spread");
  default:
    return std::string("Unknown or not supported");
  }
}

std::string to_string(KernelType kernel) {
  switch (kernel) {
  case KernelType::EMPTY:
    return std::string("Empty");
  case KernelType::COMPUTE_BOUND:
    return std::string("Compute Bound");
  case KernelType::COMPUTE_BOUND2:
    return std::string("Compute Bound 2");
  case KernelType::LOAD_IMBALANCE:
    return std::string("Load Imbalance");
  case KernelType::BUSY_WAIT:
  case KernelType::MEMORY_BOUND:
  case KernelType::COMPUTE_DGEMM:
  case KernelType::MEMORY_DAXPY:
  case KernelType::IO_BOUND:
  default:
    return std::string("Unknown or not supported");
  }
}

size_t num_dependencies(TaskGraph g, long dset, long point) {
  size_t count = 0;

  switch (g.dependence) {
  case DependenceType::TRIVIAL:
    return 0;
  case DependenceType::NO_COMM:
  case DependenceType::STENCIL_1D:
    return 1;
  case DependenceType::STENCIL_1D_PERIODIC:
    return g.max_width > 1 ? 2 : 3;
  case DependenceType::DOM:
  case DependenceType::TREE:
    return 1;
  case DependenceType::FFT:
    return 3;
  case DependenceType::ALL_TO_ALL:
    return 1;
  case DependenceType::NEAREST:
    return g.radix > 0 ? 1 : 0;
  case DependenceType::SPREAD:
  case DependenceType::RANDOM_NEAREST:
    return g.radix;
  default:
    assert(false && "unexpected dependence type");
  };

  return SIZE_MAX;
}

size_t dependencies(TaskGraph g, long dset, long point,
                    std::pair<long, long> *deps) {
  switch (g.dependence) {
  case DependenceType::TRIVIAL:
    return 0;
  case DependenceType::NO_COMM:
    deps[0] = std::pair<long, long>(point, point);
    return 1;
  case DependenceType::STENCIL_1D:
    deps[0] = std::pair<long, long>(max(0L, point - 1),
                                    min(point + 1, g.max_width - 1));
    return 1;
  case DependenceType::STENCIL_1D_PERIODIC: {
    size_t idx = 0;
    deps[idx++] = std::pair<long, long>(max(0L, point - 1),
                                        min(point + 1, g.max_width - 1));
    if (point - 1 < 0) { // Wrap around negative case
      deps[idx++] = std::pair<long, long>(g.max_width - 1, g.max_width - 1);
    }
    if (point + 1 >= g.max_width) { // Wrap around positive case
      deps[idx++] = std::pair<long, long>(0, 0);
    }
    return idx;
  }
  case DependenceType::DOM:
    deps[0] = std::pair<long, long>(max(0L, point - 1), point);
    return 1;
  case DependenceType::TREE: {
    long parent = point / 2;
    deps[0] = std::pair<long, long>(parent, parent);
    return 1;
  }
  case DependenceType::FFT: {
    size_t idx = 0;
    long d1 = point - (1 << dset);
    long d2 = point + (1 << dset);
    if (d1 >= 0) {
      deps[idx++] = std::pair<long, long>(d1, d1);
    }
    deps[idx++] = std::pair<long, long>(point, point);
    if (d2 < g.max_width) {
      deps[idx++] = std::pair<long, long>(d2, d2);
    }
    return idx;
  }
  case DependenceType::ALL_TO_ALL:
    deps[0] = std::pair<long, long>(0, g.max_width - 1);
    return 1;
  case DependenceType::NEAREST:
    if (g.radix > 0) {
      deps[0] = std::pair<long, long>(
          max(0L, point - g.radix / 2),
          min(point + (g.radix - 1) / 2, g.max_width - 1));
      return 1;
    }
    return 0;
  case DependenceType::SPREAD:
    for (long i = 0; i < g.radix; ++i) {
      long dep = (point + i * g.max_width / g.radix + (i > 0 ? dset : 0)) %
                 g.max_width;
      deps[i] = std::pair<long, long>(dep, dep);
    }
    return g.dependence;
  case DependenceType::RANDOM_NEAREST:
  default:
    assert(false && "unexpected dependence type");
  };

  return SIZE_MAX;
}

std::vector<std::pair<long, long>> dependencies(TaskGraph g, long dset,
                                                long point) {
  size_t count = num_dependencies(g, dset, point);
  std::vector<std::pair<long, long>> deps(count);
  size_t actual_count = dependencies(g, dset, point, deps.data());
  assert(actual_count <= count);
  deps.resize(actual_count);
  return deps;
}

size_t reverse_dependencies(TaskGraph g, long dset, long point,
                            std::pair<long, long> *deps) {
  long max_width = g.max_width;
  long radix = g.radix;
  switch (g.dependence) {
  case DependenceType::TRIVIAL:
    return 0;
  case DependenceType::NO_COMM:
    deps[0] = std::pair<long, long>(point, point);
    return 1;
  case DependenceType::STENCIL_1D:
    deps[0] = std::pair<long, long>(max(0L, point - 1),
                                    min(point + 1, max_width - 1));
    return 1;
  case DependenceType::STENCIL_1D_PERIODIC: {
    size_t idx = 0;
    deps[idx++] = std::pair<long, long>(max(0L, point - 1),
                                        min(point + 1, max_width - 1));
    if (point - 1 < 0) { // Wrap around negative case
      deps[idx++] = std::pair<long, long>(max_width - 1, max_width - 1);
    }
    if (point + 1 >= max_width) { // Wrap around positive case
      deps[idx++] = std::pair<long, long>(0, 0);
    }
    return idx;
  }
  case DependenceType::DOM:
    deps[0] = std::pair<long, long>(point, min(max_width - 1, point + 1));
    return 1;
  case DependenceType::TREE: {
    long child1 = point * 2;
    long child2 = point * 2 + 1;
    if (child1 < max_width && child2 < max_width) {
      deps[0] = std::pair<long, long>(child1, child2);
      return 1;
    } else if (child1 < max_width) {
      deps[0] = std::pair<long, long>(child1, child1);
      return 1;
    }
    return 0;
  }
  case DependenceType::FFT: {
    size_t idx = 0;
    long d1 = point - (1 << dset);
    long d2 = point + (1 << dset);
    if (d1 >= 0) {
      deps[idx++] = std::pair<long, long>(d1, d1);
    }
    deps[idx++] = std::pair<long, long>(point, point);
    if (d2 < max_width) {
      deps[idx++] = std::pair<long, long>(d2, d2);
    }
    return idx;
  }
  case DependenceType::ALL_TO_ALL:
    deps[0] = std::pair<long, long>(0, max_width - 1);
    return 1;
  case DependenceType::NEAREST:
    if (radix > 0) {
      deps[0] = std::pair<long, long>(max(0L, point - (radix - 1) / 2),
                                      min(point + radix / 2, max_width - 1));
      return 1;
    }
    return 0;
  case DependenceType::SPREAD:
    for (long i = 0; i < radix; ++i) {
      long dep =
          (point - i * max_width / radix - (i > 0 ? dset : 0)) % max_width;
      if (dep < 0)
        dep += max_width;
      deps[i] = std::pair<long, long>(dep, dep);
    }
    return radix;
  case DependenceType::RANDOM_NEAREST:
  default:
    assert(false && "unexpected dependence type");
  };

  return SIZE_MAX;
}

size_t num_reverse_dependencies(TaskGraph g, long dset, long point) {
  switch (g.dependence) {
  case DependenceType::TRIVIAL:
    return 0;
  case DependenceType::NO_COMM:
  case DependenceType::STENCIL_1D:
    return 1;
  case DependenceType::STENCIL_1D_PERIODIC:
    return g.max_width > 1 ? 2 : 3;
  case DependenceType::DOM:
  case DependenceType::TREE:
    return 1;
  case DependenceType::FFT:
    return 3;
  case DependenceType::ALL_TO_ALL:
    return 1;
  case DependenceType::NEAREST:
    return g.radix > 0 ? 1 : 0;
  case DependenceType::SPREAD:
  case DependenceType::RANDOM_NEAREST:
    return g.radix;
  default:
    assert(false && "unexpected dependence type");
  };

  return SIZE_MAX;
}
std::vector<std::pair<long, long>> reverse_dependencies(TaskGraph g, long dset,
                                                        long point) {
  size_t count = num_reverse_dependencies(g, dset, point);
  std::vector<std::pair<long, long>> deps(count);
  size_t actual_count = reverse_dependencies(g, dset, point, deps.data());
  assert(actual_count <= count);
  deps.resize(actual_count);
  return deps;
}

static bool needs_period(DependenceType dtype) {
  return dtype == DependenceType::SPREAD ||
         dtype == DependenceType::RANDOM_NEAREST;
}

static void checkGraphs(std::vector<TaskGraph> graphs) {
  for (auto g : graphs) {
    if (needs_period(g.dependence) && g.period == 0) {
      fprintf(stderr,
              "error: Graph type \"%s\" requires a non-zero period (specify "
              "with -period)\n",
              to_string(g.dependence).c_str());
      abort();
    } else if (!needs_period(g.dependence) && g.period != 0) {
      fprintf(stderr,
              "error: Graph type \"%s\" does not support user-configurable "
              "period\n",
              to_string(g.dependence).c_str());
      abort();
    }

    // This is required to avoid wrapping around with later dependence sets.
    long spread = (g.max_width + g.radix - 1) / g.radix;
    if (g.dependence == DependenceType::SPREAD && g.period > spread) {
      fprintf(
          stderr,
          "error: Graph type \"%s\" requires a period that is at most %ld\n",
          to_string(g.dependence).c_str(), spread);
      abort();
    }

    for (long t = 0; t < g.timesteps; ++t) {
      long offset =
          core::offset_at_timestep(g.dependence, t, g.max_width, g.timesteps);
      long width =
          core::width_at_timestep(g.dependence, t, g.max_width, g.timesteps);
      assert(offset >= 0);
      assert(width >= 0);
      assert(offset + width <= g.max_width);

      long dset = core::dependence_set_at_timestep(g.dependence, t, g.max_width,
                                                   g.period);
      assert(dset >= 0 && dset <= core::max_dependence_sets(
                                      g.dependence, g.max_width, g.period));
    }
    for (long dset = 0;
         dset < core::max_dependence_sets(g.dependence, g.max_width, g.period);
         ++dset) {
      std::map<long, std::set<long>> materialized_deps;
      for (long point = 0; point < g.max_width; ++point) {
        auto deps = dependencies(g, dset, point);
        for (auto dep : deps) {
          for (long dp = dep.first; dp <= dep.second; ++dp) {
            assert(materialized_deps[point].count(dp) == 0); // No duplicates
            materialized_deps[point].insert(dp);
          }
        }
      }

      // Reverse dependencies mirror dependencies
      for (long point = 0; point < g.max_width; ++point) {
        auto rdeps = reverse_dependencies(g, dset, point);
        for (auto rdep : rdeps) {
          for (long rdp = rdep.first; rdp <= rdep.second; ++rdp) {
            assert(materialized_deps[rdp].count(point) == 1);
          }
        }
      }
    }
  }
}

static TaskGraph default_graph(DependenceType dep, KernelType kernel) {
  TaskGraph graph;

  graph.graph_index = 0;
  graph.timesteps = 4;
  graph.max_width = 3;
  graph.dependence = dep;
  graph.radix = 3;
  graph.period = needs_period(dep) == true ? 2 : 0;
  graph.fraction_connected = 0.25;
  graph.kernel = {kernel, 1000, 16, 0.25};
  graph.output_bytes_per_task = 131072;
  graph.scratch_bytes_per_task = 0;
  graph.nb_fields = graph.timesteps;

  return graph;
}
