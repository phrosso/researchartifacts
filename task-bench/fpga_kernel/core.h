/*******************************************************************************
#  Copyright (C) 2022 Xilinx, Inc
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#
#
*******************************************************************************/
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <utility>
#include <vector>

#define max(a, b) ((a > b) ? a : b)
#define min(a, b) ((a < b) ? a : b)

#define MAX_INPUT_NUMBER 9

enum dependence_type_t {
  TRIVIAL,
  NO_COMM,
  STENCIL_1D,
  STENCIL_1D_PERIODIC,
  DOM,
  TREE,
  FFT,
  ALL_TO_ALL,
  NEAREST,
  SPREAD,
  RANDOM_NEAREST,
  RANDOM_SPREAD,
};

typedef enum kernel_type_t {
  EMPTY,
  BUSY_WAIT,
  MEMORY_BOUND,
  COMPUTE_DGEMM,
  MEMORY_DAXPY,
  COMPUTE_BOUND,
  COMPUTE_BOUND2,
  IO_BOUND,
  LOAD_IMBALANCE,
} kernel_type_t;

struct kernel_t {
  kernel_type_t type;
  long iterations;
  int samples;
  double imbalance; // amount of imbalance as a fraction of the number of
                    // iterations
};

struct task_graph_t {
  long graph_index;
  long timesteps;
  long max_width;
  dependence_type_t dependence;
  long radix;  // max number of dependencies in nearest/spread/random patterns
  long period; // period of repetition in spread/random pattern
  double fraction_connected; // fraction of connected nodes in random pattern
  kernel_t kernel;
  size_t output_bytes_per_task;
  size_t scratch_bytes_per_task;
  int nb_fields;
};

typedef dependence_type_t DependenceType;

typedef kernel_type_t KernelType;

typedef task_graph_t TaskGraph;

// methods
namespace core {
long offset_at_timestep(DependenceType dep, long timestep, long max_width,
                        long timesteps);
long width_at_timestep(DependenceType dep, long timestep, long max_width,
                       long timesteps);
long max_dependence_sets(DependenceType dep, long max_width, long period);
long dependence_set_at_timestep(DependenceType dep, long timestep,
                                long max_width, long period);
size_t num_dependencies(DependenceType dep, long max_width, long radix);
void dependencies(TaskGraph graph, long timestep, long point, long *final_deps);
} // namespace core
