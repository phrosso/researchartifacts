/*******************************************************************************
#  Copyright (C) 2022 Xilinx, Inc
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#
#
*******************************************************************************/
#include "ap_int.h"
#include "hls_math.h"

#include "core.h"
#include "kernels.h"
#include <cstdio>

TaskGraph read_graph(TaskGraph *graphs_array) {
  ap_uint<512> *graphs = reinterpret_cast<ap_uint<512> *>(graphs_array);
  ap_uint<512> p0 = graphs[0];
  ap_uint<512> p1 = graphs[1];

  union cg {
    ap_uint<128> v[7] = {0, 0, 0, 0, 0, 0, 0};
    TaskGraph T;
  } charGraph;

  charGraph.v[0] = p0.range(127, 0);
  charGraph.v[1] = p0.range(255, 128);
  charGraph.v[2] = p0.range(383, 256);
  charGraph.v[3] = p0.range(511, 384);
  charGraph.v[4] = p1.range(127, 0);
  charGraph.v[5] = p1.range(255, 128);
  charGraph.v[6] = p1.range(383, 256);

  return charGraph.T;
}

void validate_input(char *input_ptr, size_t input_bytes, long timestep,
                    long point, long last_offset, long last_width,
                    bool *input_validator, long index, long num_args) {
#pragma HLS inline off
  *input_validator = true; 
  bool result = true;
  if (index > num_args - 1)
    return;
  if (point != -1) {
    if (last_offset <= point && point < last_offset + last_width) {
      ap_uint<512> *input = reinterpret_cast<ap_uint<512> *>(input_ptr);
      int size = input_bytes / sizeof(ap_uint<512>);
      const size_t num_ele = (512 / 8) / sizeof(long);
      size_t long_size = sizeof(long) * 8;
      // verify the inputs
      for (size_t i = 0; i < size; i++) {
        ap_uint<512> value = input[i];
        ap_uint<512> p_res = 0;
        for (int j = 0; j < num_ele; j++) {
#pragma HLS unroll factor = num_ele
          int end = (j + 1) * long_size - 1;
          int start = j *long_size;
          if (j % 2 == 0) {
            if (!(value.range(end, start) == timestep - 1))
              p_res.range(end, start) = 1;
          } else {
            if (!(value.range(end, start) == point))
              p_res.range(end, start) = 1;
          }
        }
        if (p_res != 0)
          result = false;
      }
    } 
  }
  if (result == false)
    *input_validator = false;
}

void inputs(char *in0, char *in1, char *in2, char *in3, char *in4, char *in5,
            char *in6, char *in7, char *in8, long d0, long d1, long d2, long d3,
            long d4, long d5, long d6, long d7, long d8, bool *v0, bool *v1,
            bool *v2, bool *v3, bool *v4, bool *v5, bool *v6, bool *v7,
            bool *v8, long num_args, long size, long timestep, long last_offset,
            long last_width) {
// #pragma HLS dataflow
  validate_input(in8, size, timestep, d8, last_offset, last_width, v8, 8,
                 num_args);
  validate_input(in7, size, timestep, d7, last_offset, last_width, v7, 7,
                 num_args);
  validate_input(in6, size, timestep, d6, last_offset, last_width, v6, 6,
                 num_args);
  validate_input(in5, size, timestep, d5, last_offset, last_width, v5, 5,
                 num_args);
  validate_input(in4, size, timestep, d4, last_offset, last_width, v4, 4,
                 num_args);
  validate_input(in3, size, timestep, d3, last_offset, last_width, v3, 3,
                 num_args);
  validate_input(in2, size, timestep, d2, last_offset, last_width, v2, 2,
                 num_args);
  validate_input(in1, size, timestep, d1, last_offset, last_width, v1, 1,
                 num_args);
  validate_input(in0, size, timestep, d0, last_offset, last_width, v0, 0,
                 num_args);
}

void write_buffer(char *buffer, long timestep, long point, long output_bytes) {
  ap_uint<512> *output = reinterpret_cast<ap_uint<512> *>(buffer);
  int size = output_bytes / sizeof(ap_uint<512>);
  const size_t num_ele = (512 / 8) / sizeof(long);
  size_t long_size = sizeof(long) * 8;

  for (size_t i = 0; i < size; i++) {
    ap_uint<512> p_res = 0;
    for (int j = 0; j < num_ele; j++) {
#pragma HLS unroll factor = num_ele
      int end = (j + 1) * long_size - 1;
      int start = j * long_size;
      if (j % 2 == 0)
        p_res.range(end, start) = timestep;
      else
        p_res.range(end, start) = point;
    }
    output[i] = p_res;
  }
}

// Kernel called by OMPC
void task_execute(
    TaskGraph *graphs_array, // Contains all the information about the graphs
    char *in0_ptr,           // input buffer 0, if applicable
    char *in1_ptr,           // input buffer 1, if applicable
    char *in2_ptr,           // input buffer 2, if applicable
    char *in3_ptr,           // input buffer 3, if applicable
    char *in4_ptr,           // input buffer 4, if applicable
    char *in5_ptr,           // input buffer 5, if applicable
    char *in6_ptr,           // input buffer 6, if applicable
    char *in7_ptr,           // input buffer 7, if applicable
    char *in8_ptr,           // input buffer 8, if applicable
    char *output_ptr,        // output buffer
    bool *validated,         // validation flag
    int graph_idx,           // index of this graph
    int num_args,            // number of input buffers used
    long point,              // position x in the graph (point)
    long timestep            // position y in the graph (step)
) {
#pragma HLS INTERFACE m_axi depth = 1024 port = graphs_array
#pragma HLS INTERFACE m_axi depth = 131072 port = in0_ptr
#pragma HLS INTERFACE m_axi depth = 131072 port = in1_ptr
#pragma HLS INTERFACE m_axi depth = 131072 port = in2_ptr
#pragma HLS INTERFACE m_axi depth = 131072 port = in3_ptr
#pragma HLS INTERFACE m_axi depth = 131072 port = in4_ptr
#pragma HLS INTERFACE m_axi depth = 131072 port = in5_ptr
#pragma HLS INTERFACE m_axi depth = 131072 port = in6_ptr
#pragma HLS INTERFACE m_axi depth = 131072 port = in7_ptr
#pragma HLS INTERFACE m_axi depth = 131072 port = in8_ptr
#pragma HLS INTERFACE m_axi depth = 131072 port = output_ptr
#pragma HLS INTERFACE m_axi depth = 32 port = validated
#pragma HLS INTERFACE s_axilite port = graph_idx
#pragma HLS INTERFACE s_axilite port = num_args
#pragma HLS INTERFACE s_axilite port = point
#pragma HLS INTERFACE s_axilite port = timestep
#pragma HLS INTERFACE s_axilite port = return

  validated[0] = true;
  validated[1] = true;
  validated[2] = true;
  validated[3] = true;
  validated[4] = true;

  // Gets the graph object
  TaskGraph graph = read_graph(graphs_array);

  // Generic graph validations
  if (!(0 <= timestep && timestep < graph.timesteps)) {
    validated[0] = false;
  }

  long offset = core::offset_at_timestep(graph.dependence, timestep,
                                         graph.max_width, graph.timesteps);
  long width = core::width_at_timestep(graph.dependence, timestep,
                                       graph.max_width, graph.timesteps);

  if (!(offset <= point && point < offset + width)) {
    validated[1] = false; 
  }

  // Gets an array specifying the dependency for each input
  long deps[MAX_INPUT_NUMBER] = {-1, -1, -1, -1, -1, -1, -1, -1, -1};
  core::dependencies(graph, timestep, point, deps);

  long last_offset = core::offset_at_timestep(graph.dependence, timestep - 1,
                                              graph.max_width, graph.timesteps);
  long last_width = core::width_at_timestep(graph.dependence, timestep - 1,
                                            graph.max_width, graph.timesteps);

  // Evaluate each input based on the dependencies we just got
  long size = graph.output_bytes_per_task;
  bool v0, v1, v2, v3, v4, v5, v6, v7, v8;
  inputs(in0_ptr, in1_ptr, in2_ptr, in3_ptr, in4_ptr, in5_ptr, in6_ptr, in7_ptr,
         in8_ptr, deps[0], deps[1], deps[2], deps[3], deps[4], deps[5], deps[6],
         deps[7], deps[8], &v0, &v1, &v2, &v3, &v4, &v5, &v6, &v7, &v8,
         num_args, size, timestep, last_offset, last_width);

  validated[2] = v0 && v1 && v2 && v3 && v4 && v5 && v6 && v7 && v8;

  // Validate and generate output
  if (!(graph.output_bytes_per_task >= (2 * sizeof(long)))) {
    validated[3] = false;
  }

  write_buffer(output_ptr, timestep, point, graph.output_bytes_per_task);

  // Execute the kernel
  long iterations = graph.kernel.iterations;
  double imbalance = graph.kernel.imbalance;
 
  switch (graph.kernel.type) {
  case kernel_type_t::COMPUTE_BOUND: {
    double value = kernel::compute_bound(iterations);
    if (value < 128) {
      validated[4] = false;
    }
  } break;
  case kernel_type_t::COMPUTE_BOUND2: {
    double value = kernel::compute_bound_2(iterations);
    if (value < 128) {
      validated[4] = false;
    }
  } break;
  case kernel_type_t::LOAD_IMBALANCE: {
    double value =kernel::imbalance(iterations, imbalance, graph_idx, timestep, point);
    if (value <= 128) {
      validated[4] = false;
    }
  } break;
  case kernel_type_t::EMPTY:
  case kernel_type_t::MEMORY_BOUND:
  case kernel_type_t::COMPUTE_DGEMM:
  case kernel_type_t::MEMORY_DAXPY:
  case kernel_type_t::BUSY_WAIT:
  case kernel_type_t::IO_BOUND:
  default:
    kernel::empty();
    break;
  }
}
