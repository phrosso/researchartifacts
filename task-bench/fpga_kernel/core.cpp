/*******************************************************************************
#  Copyright (C) 2022 Xilinx, Inc
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#
#
*******************************************************************************/

#include <cmath>
#include "hls_math.h"

#include "core.h"

namespace core {
long offset_at_timestep(DependenceType dep, long timestep, long max_width,
                        long timesteps) {
  #pragma HLS inline off
  if (timestep < 0) {
    return 0;
  }

  switch (dep) {
  case DependenceType::TRIVIAL:
  case DependenceType::NO_COMM:
  case DependenceType::STENCIL_1D:
  case DependenceType::STENCIL_1D_PERIODIC:
    return 0;
  case DependenceType::DOM:
    return max(0L, timestep + max_width - timesteps);
  case DependenceType::TREE:
  case DependenceType::FFT:
  case DependenceType::ALL_TO_ALL:
  case DependenceType::NEAREST:
  case DependenceType::SPREAD:
  case DependenceType::RANDOM_NEAREST:
  case DependenceType::RANDOM_SPREAD:
    return 0;
  default:
    return 0;
  };
}

long width_at_timestep(DependenceType dep, long timestep, long max_width,
                       long timesteps) {
  #pragma HLS inline off
  if (timestep < 0) {
    return 0;
  }

  switch (dep) {
  case DependenceType::TRIVIAL:
  case DependenceType::NO_COMM:
  case DependenceType::STENCIL_1D:
  case DependenceType::STENCIL_1D_PERIODIC:
    return max_width;
  case DependenceType::DOM:
    return min(max_width, min(timestep + 1, timesteps - timestep));
  case DependenceType::TREE:
    return min(max_width, 1L << min(timestep, 62L));
  case DependenceType::FFT:
  case DependenceType::ALL_TO_ALL:
  case DependenceType::NEAREST:
  case DependenceType::SPREAD:
  case DependenceType::RANDOM_NEAREST:
  case DependenceType::RANDOM_SPREAD:
    return max_width;
  default:
    return 0;
  };
}

long max_dependence_sets(DependenceType dep, long max_width, long period) {
  #pragma HLS inline off
  switch (dep) {
  case DependenceType::TRIVIAL:
  case DependenceType::NO_COMM:
  case DependenceType::STENCIL_1D:
  case DependenceType::STENCIL_1D_PERIODIC:
  case DependenceType::DOM:
  case DependenceType::TREE:
    return 1;
  case DependenceType::FFT:
  {
    double log2_r = hls::log2((float) max_width);
    return (long)hls::ceil(log2_r);
  }
  case DependenceType::ALL_TO_ALL:
  case DependenceType::NEAREST:
    return 1;
  case DependenceType::SPREAD:
  case DependenceType::RANDOM_NEAREST:
  case DependenceType::RANDOM_SPREAD:
    return period;
  default:
    return 0;
  };
}

long dependence_set_at_timestep(DependenceType dep, long timestep,
                                long max_width, long period) {
  #pragma HLS inline off
  long max_dep_set = max_dependence_sets(dep, max_width, period);

  switch (dep) {
  case DependenceType::TRIVIAL:
  case DependenceType::NO_COMM:
  case DependenceType::STENCIL_1D:
  case DependenceType::STENCIL_1D_PERIODIC:
  case DependenceType::DOM:
  case DependenceType::TREE:
    return 0;
  case DependenceType::FFT:
    return (timestep + max_dep_set - 1) % max_dep_set;
  case DependenceType::ALL_TO_ALL:
  case DependenceType::NEAREST:
    return 0;
  case DependenceType::SPREAD:
  case DependenceType::RANDOM_NEAREST:
  case DependenceType::RANDOM_SPREAD:
    return timestep % max_dep_set;
  default:
    return 0;
  };
}

size_t num_dependencies(DependenceType dep, long max_width, long radix) {
  #pragma HLS inline off
  size_t count = 0;

  switch (dep) {
  case DependenceType::TRIVIAL:
    return 0;
  case DependenceType::NO_COMM:
  case DependenceType::STENCIL_1D:
    return 1;
  case DependenceType::STENCIL_1D_PERIODIC:
    return max_width > 1 ? 2 : 3;
  case DependenceType::DOM:
  case DependenceType::TREE:
    return 1;
  case DependenceType::FFT:
    return 3;
  case DependenceType::ALL_TO_ALL:
    return 1;
  case DependenceType::NEAREST:
    return radix > 0 ? 1 : 0;
  case DependenceType::SPREAD:
  case DependenceType::RANDOM_NEAREST:
    return radix;
  default:
    break;
  };

  return SIZE_MAX;
}

void dependencies(TaskGraph graph, long timestep, long point,
                  long *final_deps) {
  #pragma HLS inline off
  long last_offset = core::offset_at_timestep(graph.dependence, timestep - 1,
                                              graph.max_width, graph.timesteps);
  long last_width = core::width_at_timestep(graph.dependence, timestep - 1,
                                            graph.max_width, graph.timesteps);
  long max_width = graph.max_width;
  long radix = graph.radix;

  long dset = core::dependence_set_at_timestep(graph.dependence, timestep,
                                               graph.max_width, graph.period);
  size_t max_deps =
      core::num_dependencies(graph.dependence, graph.max_width, graph.radix);

  std::pair<long, long> deps[3];
  int num_deps = 0;
  switch (graph.dependence) {
  case DependenceType::TRIVIAL: {
  } break;
  case DependenceType::NO_COMM: {
    deps[0] = std::pair<long, long>(point, point);
    num_deps = 1;
  } break;
  case DependenceType::STENCIL_1D: {
    deps[0] = std::pair<long, long>(max(0L, point - 1),
                                    min(point + 1, max_width - 1));
    num_deps = 1;
  } break;
  case DependenceType::STENCIL_1D_PERIODIC: {
    size_t idx = 0;
    deps[idx++] = std::pair<long, long>(max(0L, point - 1),
                                        min(point + 1, max_width - 1));
    if (point - 1 < 0) { // Wrap around negative case
      deps[idx++] = std::pair<long, long>(max_width - 1, max_width - 1);
    }
    if (point + 1 >= max_width) { // Wrap around positive case
      deps[idx++] = std::pair<long, long>(0, 0);
    }
    num_deps = idx;
  } break;
  case DependenceType::DOM: {
    deps[0] = std::pair<long, long>(max(0L, point - 1), point);
    num_deps = 1;

  } break;
  case DependenceType::TREE: {
    long parent = point / 2;
    deps[0] = std::pair<long, long>(parent, parent);
    num_deps = 1;
  } break;
  case DependenceType::FFT: {
    size_t idx = 0;
    long d1 = point - (1 << dset);
    long d2 = point + (1 << dset);
    if (d1 >= 0) {
      deps[idx++] = std::pair<long, long>(d1, d1);
    }
    deps[idx++] = std::pair<long, long>(point, point);
    if (d2 < max_width) {
      deps[idx++] = std::pair<long, long>(d2, d2);
    }
    num_deps = idx;
  } break;
  case DependenceType::ALL_TO_ALL: {
    deps[0] = std::pair<long, long>(0, max_width - 1);
    num_deps = 1;

  } break;
  case DependenceType::NEAREST: {
    if (radix > 0) {
      deps[0] =
          std::pair<long, long>(max(0L, point - radix / 2),
                                min(point + (radix - 1) / 2, max_width - 1));
      num_deps = 1;
    }
  } break;
  case DependenceType::SPREAD: {
    for (long i = 0; i < radix; ++i) {
      long dep =
          (point + i * max_width / radix + (i > 0 ? dset : 0)) % max_width;
      deps[i] = std::pair<long, long>(dep, dep);
    }
    num_deps = radix;
  } break;
  case DependenceType::RANDOM_NEAREST:
  default:
    break;
  };

  long offset = core::offset_at_timestep(graph.dependence, timestep,
                                         graph.max_width, graph.timesteps);
  long width = core::width_at_timestep(graph.dependence, timestep,
                                       graph.max_width, graph.timesteps);

  size_t idx = 0;
  for (size_t span = 0; span < num_deps; span++)
    for (long dep = deps[span].first; dep <= deps[span].second; dep++)
      final_deps[idx++] = dep;
}
} // namespace core