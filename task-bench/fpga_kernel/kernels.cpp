/*******************************************************************************
#  Copyright (C) 2022 Xilinx, Inc
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#
#
*******************************************************************************/

#include "kernels.h"
// #include "hls_print.h"

#include <ap_int.h>
#include <math.h>
#include <stdlib.h>

unsigned int pseudo_random(unsigned int seed) {
  ap_uint<32> lfsr;
  lfsr = seed;

  // do two times
  bool b_32 = lfsr.get_bit(32 - 32);
  bool b_22 = lfsr.get_bit(32 - 22);
  bool b_2 = lfsr.get_bit(32 - 2);
  bool b_1 = lfsr.get_bit(32 - 1);
  bool new_bit = b_32 ^ b_22 ^ b_2 ^ b_1;
  lfsr = lfsr >> 1;
  lfsr.set_bit(31, new_bit);

  b_32 = lfsr.get_bit(32 - 32);
  b_22 = lfsr.get_bit(32 - 22);
  b_2 = lfsr.get_bit(32 - 2);
  b_1 = lfsr.get_bit(32 - 1);
  new_bit = b_32 ^ b_22 ^ b_2 ^ b_1;
  lfsr = lfsr >> 1;
  lfsr.set_bit(31, new_bit);

  return lfsr.to_uint();
}

namespace kernel {
void empty() {
#pragma HLS inline off
  // Do nothing...
}

double compute_bound(long iterations) {
#pragma HLS inline off
  double A[64];

  for (int i = 0; i < 64; i++) {
    #pragma HLS pipeline off
    A[i] = 1.2345;
  }

  for (long iter = 0; iter < iterations; iter++) {
    for (int i = 0; i < 64; i++) {
      A[i] = A[i] * A[i] + A[i];
    }
  }

  double *C = (double *)A;
  double dot = 0.0;
  for (int i = 0; i < 64; i++) {
    #pragma HLS pipeline off
    dot += C[i];
  }
  // hls::print("%f\n", dot);
  return dot;
}

double compute_bound_2(long iterations) {
#pragma HLS inline off
  constexpr size_t N = 32;
  double A[N] = {0};
  double B[N] = {0};
  double C[N] = {0};

  for (size_t i = 0; i < N; ++i) {
    #pragma HLS pipeline off
    A[i] = 1.2345;
    B[i] = 1.010101;
  }

  for (long iter = 0; iter < iterations; iter++) {
    for (size_t i = 0; i < N; ++i) {
      C[i] = C[i] + (A[i] * B[i]);
    }
  }

  double sum = 0;
  for (size_t i = 0; i < N; ++i) {
    #pragma HLS pipeline off
    sum += C[i];
  }

  // hls::print("%f\n", sum);
  return sum;
}

double imbalance(long iterations, double imbalance, long graph_index,
               long timestep, long point) {
#pragma HLS inline off
  // get the number of iterations
  // long seed[3] = {graph_index, timestep, point};
  unsigned int seed = point + (timestep << 8) + (graph_index << 16);
  unsigned int max_v = 0xFFFFFFFF;
  double value = pseudo_random(seed) / (1.0 * max_v); // value between 0 and 1

  long imb_iterations =
      (long)round((1 + (value - 0.5) * imbalance) * iterations);

  // hls::print("K_imb: %d\n", (int)imb_iterations);
  return compute_bound(imb_iterations);
}
} // namespace kernel
