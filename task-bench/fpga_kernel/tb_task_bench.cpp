#include <cassert>
#include <cstring>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <iomanip>
#include <chrono>

#include "tb_task_bench.h"

void test_graph(std::vector<TaskGraph> graphs) {
  std::string dependence_type = to_string(graphs[0].dependence);
  std::string kernel_type = to_string(graphs[0].kernel.type);

  checkGraphs(graphs);

  // Create the Task Graphs and the information of each task
  matrix_t *matrix = (matrix_t *)malloc(sizeof(matrix_t) * graphs.size());
  for (unsigned i = 0; i < graphs.size(); i++) {
    TaskGraph &graph = graphs[i];
    matrix[i].M = graph.nb_fields;
    matrix[i].N = graph.max_width;
    int number_point = matrix[i].M * matrix[i].N;
    assert(!posix_memalign((void **)&matrix[i].data, 4096,
                           sizeof(tile_t) * number_point));
    for (int j = 0; j < number_point; j++) {
      assert(!posix_memalign((void **)&matrix[i].data[j].output_buff, 4096,
                             sizeof(char) * graph.output_bytes_per_task));
    }
  }

  TaskGraph *graphs_array;
  assert(!posix_memalign((void **)&graphs_array, 4096, 2 * sizeof(TaskGraph)));

  memcpy(graphs_array, graphs.data(), sizeof(TaskGraph) * graphs.size());

  // Run the task graph
  for (unsigned graph_idx = 0; graph_idx < graphs.size(); graph_idx++) {
    std::cout << "Graph: (" << dependence_type << " - " << kernel_type
              << ") - Started" << std::endl;
    const TaskGraph &graph = graphs[graph_idx];
    tile_t *mat = matrix[graph_idx].data;
    size_t graph_out = graph.output_bytes_per_task;

    char *dummy_input;
    assert(
        !posix_memalign((void **)&dummy_input, 4096, graph_out * sizeof(char)));

    bool *validated;
    assert(
        !posix_memalign((void **)&validated, 4096, 32 * sizeof(bool)));

    for (int y = 0; y < graph.timesteps; y++) {
      long offset = core::offset_at_timestep(graph.dependence, y,
                                             graph.max_width, graph.timesteps);
      long width = core::width_at_timestep(graph.dependence, y, graph.max_width,
                                           graph.timesteps);
      long dset = core::dependence_set_at_timestep(
          graph.dependence, y, graph.max_width, graph.period);
      int nb_fields = graph.nb_fields;

      task_args_t args[MAX_NUM_ARGS];
      int num_args = 0;
      int curr_arg_idx = 0;

      for (int x = offset; x <= offset + width - 1; x++) {
        std::vector<std::pair<long, long>> deps = dependencies(graph, dset, x);
        num_args = 0;
        curr_arg_idx = 0;

        // Calculate the number of dependencies of the task in position (y,x)
        num_args = 1;
        args[curr_arg_idx].x = x;
        args[curr_arg_idx].y = y % nb_fields;
        curr_arg_idx++;
        if (deps.size() != 0 && y != 0) {
          long last_offset = core::offset_at_timestep(
              graph.dependence, y - 1, graph.max_width, graph.timesteps);
          long last_width = core::width_at_timestep(
              graph.dependence, y - 1, graph.max_width, graph.timesteps);
          for (std::pair<long, long> dep : deps) {
            num_args += dep.second - dep.first + 1;
            for (int i = dep.first; i <= dep.second; i++) {
              if (i >= last_offset && i < last_offset + last_width) {
                args[curr_arg_idx].x = i;
                args[curr_arg_idx].y = (y - 1) % nb_fields;
                curr_arg_idx++;
              } else {
                num_args--;
              }
            }
          }
        }
        assert(num_args == curr_arg_idx);

        // Calculate the position of the task to execute
        int x0 = args[0].x;
        int y0 = args[0].y;
        int point0 = y0 * graph.max_width + x0;

        // Get the pointer to the task output data
        tile_t *tile_in0 = &mat[point0];
        char *output_ptr = (char *)tile_in0->output_buff;

        // Create the references to inputs, starting with the dummy input
        char *input_ptr0 = (char *)dummy_input;
        char *input_ptr1 = (char *)dummy_input;
        char *input_ptr2 = (char *)dummy_input;
        char *input_ptr3 = (char *)dummy_input;
        char *input_ptr4 = (char *)dummy_input;
        char *input_ptr5 = (char *)dummy_input;
        char *input_ptr6 = (char *)dummy_input;
        char *input_ptr7 = (char *)dummy_input;
        char *input_ptr8 = (char *)dummy_input;

        switch (num_args) {
        case 10: {
          int point9 = args[9].y * graph.max_width + args[9].x;
          tile_t *tile_in9 = &mat[point9];
          input_ptr8 = (char *)tile_in9->output_buff;
        };
        case 9: {
          int point8 = args[8].y * graph.max_width + args[8].x;
          tile_t *tile_in8 = &mat[point8];
          input_ptr7 = (char *)tile_in8->output_buff;
        };
        case 8: {
          int point7 = args[7].y * graph.max_width + args[7].x;
          tile_t *tile_in7 = &mat[point7];
          input_ptr6 = (char *)tile_in7->output_buff;
        };
        case 7: {
          int point6 = args[6].y * graph.max_width + args[6].x;
          tile_t *tile_in6 = &mat[point6];
          input_ptr5 = (char *)tile_in6->output_buff;
        };
        case 6: {
          int point5 = args[5].y * graph.max_width + args[5].x;
          tile_t *tile_in5 = &mat[point5];
          input_ptr4 = (char *)tile_in5->output_buff;
        };
        case 5: {
          int point4 = args[4].y * graph.max_width + args[4].x;
          tile_t *tile_in4 = &mat[point4];
          input_ptr3 = (char *)tile_in4->output_buff;
        };
        case 4: {
          int point3 = args[3].y * graph.max_width + args[3].x;
          tile_t *tile_in3 = &mat[point3];
          input_ptr2 = (char *)tile_in3->output_buff;
        };
        case 3: {
          int point2 = args[2].y * graph.max_width + args[2].x;
          tile_t *tile_in2 = &mat[point2];
          input_ptr1 = (char *)tile_in2->output_buff;
        };
        case 2: {
          int point1 = args[1].y * graph.max_width + args[1].x;
          tile_t *tile_in1 = &mat[point1];
          input_ptr0 = (char *)tile_in1->output_buff;
        } break;
        case 1: {
          input_ptr0 = output_ptr;
        } break;
        default:
          assert(false && "unexpected num_args");
        }

        validated[0] = true;
        validated[1] = true;
        validated[2] = true;
        validated[3] = true;
        validated[4] = true;

        // Call task_execute
        auto g_start = std::chrono::steady_clock::now();
        task_execute(graphs_array, input_ptr0, input_ptr1, input_ptr2,
                     input_ptr3, input_ptr4, input_ptr5, input_ptr6, input_ptr7,
                     input_ptr8, output_ptr, validated, graph_idx, num_args, x0,
                     y0);
        auto g_finish = std::chrono::steady_clock::now();
        std::cout << std::fixed << std::setprecision(8)
                  << (std::chrono::duration<double>(g_finish - g_start)).count()
                  << "\n";

        if (!validated[0])
          assert(false && "Error when verifying Timestep");

        if (!validated[1])
          assert(false && "Error when verifying Point");

        if (!validated[2])
          assert(false && "Error when verifying Inputs");

        if (!validated[3])
          assert(false && "Error when generating Outputs");

        if (!validated[4])
          assert(false && "Error when executing Kernel");
      }
    }

    free(dummy_input);
    free(validated);
  }
  // Deallocate memory block used to storethe information of the Task Graph
  for (unsigned i = 0; i < graphs.size(); i++) {
    for (int j = 0; j < matrix[i].M * matrix[i].N; j++) {
      free(matrix[i].data[j].output_buff);
      matrix[i].data[j].output_buff = NULL;
    }
    free(matrix[i].data);
    matrix[i].data = NULL;
  }

  free(graphs_array);

  free(matrix);

  std::cout << "Graph: (" << dependence_type << " - " << kernel_type
            << ") - Finished." << std::endl;
}

int main() {
  std::vector<DependenceType> deps = {
      // DependenceType::TRIVIAL,    
      // DependenceType::NO_COMM,
      // DependenceType::STENCIL_1D, 
      // DependenceType::STENCIL_1D_PERIODIC,
      // DependenceType::DOM,        
      // DependenceType::TREE,
      DependenceType::FFT,        
      // DependenceType::ALL_TO_ALL,
      // DependenceType::NEAREST
  };
  std::vector<KernelType> kernels = {
      KernelType::EMPTY, 
      // KernelType::COMPUTE_BOUND, 
      // KernelType::COMPUTE_BOUND2,
      // KernelType::LOAD_IMBALANCE
  };

  for (size_t dep = 0; dep < deps.size(); dep++)
    for (size_t kernel = 0; kernel < kernels.size(); kernel++)
      test_graph({default_graph(deps[dep], kernels[kernel])});
  // test_graph({default_graph(deps[0], kernels[0])});
  
  std::cout << "All Tests Done!" << std::endl;

  return 0;
}