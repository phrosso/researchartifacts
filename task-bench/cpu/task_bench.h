/*******************************************************************************
#  Copyright (C) 2022 Xilinx, Inc
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#
#
*******************************************************************************/

#include "core.h"

void task_execute(
    TaskGraph *graphs_array, // Contains all the information about the graphs
    char *in0_ptr,           // input buffer 0, if applicable
    char *in1_ptr,           // input buffer 1, if applicable
    char *in2_ptr,           // input buffer 2, if applicable
    char *in3_ptr,           // input buffer 3, if applicable
    char *in4_ptr,           // input buffer 4, if applicable
    char *in5_ptr,           // input buffer 5, if applicable
    char *in6_ptr,           // input buffer 6, if applicable
    char *in7_ptr,           // input buffer 7, if applicable
    char *in8_ptr,           // input buffer 8, if applicable
    char *output_ptr,        // output buffer
    bool *validated,         // validation flag
    int graph_idx,           // index of this graph
    int num_args,            // number of input buffers used
    long point,              // position x in the graph (point)
    long timestep            // position y in the graph (step)
);