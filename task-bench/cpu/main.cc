// C headers
#include <assert.h>
#include <math.h>
#include <omp.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>

// C++ headers
#include <algorithm>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>

// Project headers
#include "core.h"
#include "timer.h"

#include "task_bench.h"

#define MAX_NUM_ARGS 10
#define MAX_INPUT_NUMBER 9

#define max(a, b) ((a > b) ? a : b)
#define min(a, b) ((a < b) ? a : b)

/*
Structure to store the position of a task in the task graph.
  x: represents the column
  y: represents the row
*/
struct task_args_t {
  int x;
  int y;
};

/*
Structure to store the information of a task
  output_buff: Stores the output information of the task when it finishes being executed.
*/
struct tile_t {
  float dep;
  char *output_buff;
};

/*
Structure to store the task graph 
  data: Array that contains the pointers for the information of each task
  M: Heigth
  N: Width
*/
struct matrix_t {
  tile_t *data;
  int M;
  int N;
};

#pragma omp declare variant(task_execute) match(device = {arch(alveo)})
void task_execute_sw(
    TaskGraph *graphs_array, // Contains all the information about the graphs
    char *in0_ptr,           // input buffer 0, if applicable
    char *in1_ptr,           // input buffer 1, if applicable
    char *in2_ptr,           // input buffer 2, if applicable
    char *in3_ptr,           // input buffer 3, if applicable
    char *in4_ptr,           // input buffer 4, if applicable
    char *in5_ptr,           // input buffer 5, if applicable
    char *in6_ptr,           // input buffer 6, if applicable
    char *in7_ptr,           // input buffer 7, if applicable
    char *in8_ptr,           // input buffer 8, if applicable
    char *output_ptr,        // output buffer
    bool *validated,         // validation flag
    int graph_idx,           // index of this graph
    int num_args,            // number of input buffers used
    long point,              // position x in the graph (point)
    long timestep            // position y in the graph (step)
);

// methods
namespace core {
long offset_at_timestep(DependenceType dep, long timestep, long max_width,
                        long timesteps);
long width_at_timestep(DependenceType dep, long timestep, long max_width,
                       long timesteps);
long max_dependence_sets(DependenceType dep, long max_width, long period);
long dependence_set_at_timestep(DependenceType dep, long timestep,
                                long max_width, long period);
size_t num_dependencies(DependenceType dep, long max_width, long radix);
void dependencies(TaskGraph graph, long timestep, long point, long *final_deps);
} // namespace core

// Kernels prototypes
namespace kernel {
void empty();
double compute_bound(long iterations);
double compute_bound_2(long interations);
double imbalance(long iterations, double imbalance, long graph_index,
               long timestep, long point);
} // namespace kernel

int main(int argc, char **argv) {
  // Create the app to store the information of the Task Graphs
  App new_app(argc, argv);
  std::vector<TaskGraph> graphs = new_app.graphs;

  // Create the Task Graphs and the information of each task
  matrix_t *matrix = (matrix_t *)malloc(sizeof(matrix_t) * graphs.size());
  for(unsigned i = 0; i<graphs.size(); i++){
    TaskGraph &graph = graphs[i];
    matrix[i].M = graph.nb_fields;
    matrix[i].N = graph.max_width;
    int number_point= matrix[i].M * matrix[i].N;
    matrix[i].data = (tile_t*)malloc(sizeof(tile_t) * number_point);
    for (int j = 0; j < number_point; j++) {
      matrix[i].data[j].output_buff = (char *)malloc(sizeof(char) * graph.output_bytes_per_task);
    }
  }

  TaskGraph *graphs_array;
  assert(!posix_memalign((void **)&graphs_array, 4096, 1024 * sizeof(TaskGraph)));
  memcpy(graphs_array, graphs.data(), sizeof(TaskGraph) * graphs.size());

  // Show the information of the task graphs
  new_app.display();
  
#pragma omp parallel
#pragma omp master
  {
    #pragma omp target 
    printf("start\n");
  }

  double start = omp_get_wtime();

#pragma omp parallel
#pragma omp master
  {
    // Transfer the graph data
    #pragma omp target enter data nowait                                       \
        map(to: graphs_array[:graphs.size()])                                  \
        depend(out: *graphs_array)

    // Run the task graph in parallel
    for (unsigned graph_idx = 0; graph_idx < graphs.size(); graph_idx++) {
      const TaskGraph &graph = graphs[graph_idx];
      tile_t *mat = matrix[graph_idx].data;
      size_t graph_out = graph.output_bytes_per_task;

      char *dummy_input;
      assert(!posix_memalign((void **)&dummy_input, 4096,
                             graph_out * sizeof(char)));

      // Transfer the dummy vector
      #pragma omp target enter data nowait                                      \
          map(to: dummy_input[:graph_out])                                      \
          depend(out: *dummy_input)


      // Alloc the data of each task on-demand using the memory management
      // (asynchronous data maps).
      int number_point = matrix[graph_idx].M * matrix[graph_idx].N;
      for (size_t i = 0; i < number_point; i++) {
        tile_t *tile_in = &mat[i];
        char *output_ptr = (char*)tile_in->output_buff;
        #pragma omp target enter data nowait                                   \
            map(alloc: output_ptr[:graph_out])                                 \
            depend(out: *output_ptr)
      }

      for (int y = 0; y < graph.timesteps; y++) {
        long offset = graph.offset_at_timestep(y);
        long width = graph.width_at_timestep(y);
        long dset = graph.dependence_set_at_timestep(y);
        int nb_fields = graph.nb_fields;

        task_args_t args[MAX_NUM_ARGS];
        int num_args = 0;
        int curr_arg_idx = 0;

        for (int x = offset; x <= offset+width-1; x++) {
          std::vector<std::pair<long, long> > deps = graph.dependencies(dset, x);
          num_args = 0;
          curr_arg_idx = 0;

          // Calculate the number of dependencies of the task in position (y,x)
          num_args = 1;
          args[curr_arg_idx].x = x;
          args[curr_arg_idx].y = y % nb_fields;
          curr_arg_idx++;
          if (deps.size() != 0 && y != 0) {
            long last_offset = graph.offset_at_timestep(y - 1);
            long last_width = graph.width_at_timestep(y - 1);
            for (std::pair<long, long> dep : deps) {
              num_args += dep.second - dep.first + 1;
              for (int i = dep.first; i <= dep.second; i++) {
                if (i >= last_offset && i < last_offset + last_width) {
                  args[curr_arg_idx].x = i;
                  args[curr_arg_idx].y = (y - 1) % nb_fields;
                  curr_arg_idx++;
                } else {
                  num_args--;
                }
              }
            }
          }
          assert(num_args == curr_arg_idx);

          // Calculate the position of the task to execute
          int x0 = args[0].x;
          int y0 = args[0].y;
          int point0 = y0 * graph.max_width + x0;

          // Get the pointer to the task output data
          tile_t *tile_in0 = &mat[point0];
          char *output_ptr = (char *)tile_in0->output_buff;

          // Create the references to inputs, starting with the dummy input
          char *input_ptr0 = (char *)dummy_input;
          char *input_ptr1 = (char *)dummy_input;
          char *input_ptr2 = (char *)dummy_input;
          char *input_ptr3 = (char *)dummy_input;
          char *input_ptr4 = (char *)dummy_input;
          char *input_ptr5 = (char *)dummy_input;
          char *input_ptr6 = (char *)dummy_input;
          char *input_ptr7 = (char *)dummy_input;
          char *input_ptr8 = (char *)dummy_input;

          switch (num_args) {
          case 10: {
            int point9 = args[9].y * graph.max_width + args[9].x;
            tile_t *tile_in9 = &mat[point9];
            input_ptr8 = (char *)tile_in9->output_buff;
          };
          case 9: {
            int point8 = args[8].y * graph.max_width + args[8].x;
            tile_t *tile_in8 = &mat[point8];
            input_ptr7 = (char *)tile_in8->output_buff;
          };
          case 8: {
            int point7 = args[7].y * graph.max_width + args[7].x;
            tile_t *tile_in7 = &mat[point7];
            input_ptr6 = (char *)tile_in7->output_buff;
          };
          case 7: {
            int point6 = args[6].y * graph.max_width + args[6].x;
            tile_t *tile_in6 = &mat[point6];
            input_ptr5 = (char *)tile_in6->output_buff;
          };
          case 6: {
            int point5 = args[5].y * graph.max_width + args[5].x;
            tile_t *tile_in5 = &mat[point5];
            input_ptr4 = (char *)tile_in5->output_buff;
          };
          case 5: {
            int point4 = args[4].y * graph.max_width + args[4].x;
            tile_t *tile_in4 = &mat[point4];
            input_ptr3 = (char *)tile_in4->output_buff;
          };
          case 4: {
            int point3 = args[3].y * graph.max_width + args[3].x;
            tile_t *tile_in3 = &mat[point3];
            input_ptr2 = (char *)tile_in3->output_buff;
          };
          case 3: {
            int point2 = args[2].y * graph.max_width + args[2].x;
            tile_t *tile_in2 = &mat[point2];
            input_ptr1 = (char *)tile_in2->output_buff;
          };
          case 2: {
            int point1 = args[1].y * graph.max_width + args[1].x;
            tile_t *tile_in1 = &mat[point1];
            input_ptr0 = (char *)tile_in1->output_buff;
          } break;
          case 1: {
            input_ptr0 = output_ptr;
          } break;
          default:
            assert(false && "unexpected num_args");
          }

          // Validation
          bool *validated;
          assert(!posix_memalign((void **)&validated, 4096, 5 * sizeof(bool)));
          validated[0] = true;
          validated[1] = true;
          validated[2] = true;
          validated[3] = true;
          validated[4] = true;

          // Now that the inputs are defined, we can create a target task
          #pragma omp target nowait                                      \
            map(tofrom: validated[:5])                                   \
            depend(in: *graphs_array)                                    \
            depend(in: *input_ptr0)                                      \
            depend(in: *input_ptr1)                                      \
            depend(in: *input_ptr2)                                      \
            depend(in: *input_ptr3)                                      \
            depend(in: *input_ptr4)                                      \
            depend(in: *input_ptr5)                                      \
            depend(in: *input_ptr6)                                      \
            depend(in: *input_ptr7)                                      \
            depend(in: *input_ptr8)                                      \
            depend(inout: *output_ptr)                                   \
            depend(inout: *validated)                                    \
            firstprivate(graph_idx, num_args, x0, y0)
          {
            task_execute_sw(graphs_array, input_ptr0, input_ptr1, input_ptr2,
                            input_ptr3, input_ptr4, input_ptr5, input_ptr6,
                            input_ptr7, input_ptr8, output_ptr, validated,
                            graph_idx, num_args, x0, y0);
          }
          // #pragma omp target nowait                                      \
          //   map(tofrom: validated[:54])                                   \
          //   depend(inout: *validated)
          // {
          //   int count = 0;
          //   if (!validated[0])
          //     // assert(false && "Failed in the timestep verification.");
          //     printf("Failed in the timestep verification.\n");

          //   if (!validated[1])
          //     // assert(false && "Failed in the point verification.");
          //     printf("Failed in the point verification.\n");

          //   if (!validated[2]) 
          //     // assert(false && "Failed in the inputs verification.");
          //     printf("Failed in the inputs verification.\n");
        
          //   if (!validated[3])
          //     // assert(false && "Failed in the create output verification.");
          //     printf("Failed in the create output verification.\n");

          //   if (!validated[4])
          //     // count++;
          //     // assert(false && "Failed in the kernel execution verification.");
          //     printf("Failed in the kernel execution verification.\n");
          // }
        }
      }

      // Delete the data of each task on-demand using (asynchronous data maps)
      // after their respective tasks have used them.
      for (size_t i = 0; i < number_point; i++) {
        tile_t *tile_in = &mat[i];
        char *output_ptr = (char *)tile_in->output_buff;
        #pragma omp target exit data nowait                                    \
                map(release: output_ptr[:graph_out])                           \
                depend(inout: *output_ptr)
      }
    
      // Release dummy vector
      #pragma omp target exit data nowait                                      \
                map(release: dummy_input[:graph_out])                          \
                depend(inout: *dummy_input)
    }

    // Release graph
    #pragma omp target exit data nowait                                        \
              map(release: graphs_array[:graphs.size()])                       \
              depend(inout: *graphs_array)
  }

  double end = omp_get_wtime();
  double elapsed = end -start;
  // Show the report of how the implementation was executed
  new_app.report_timing(elapsed);

  // Deallocate memory block used to storethe information of the Task Graph
  for (unsigned i = 0; i < graphs.size(); i++) {
    for (int j = 0; j < matrix[i].M * matrix[i].N; j++) {
      free(matrix[i].data[j].output_buff);
      matrix[i].data[j].output_buff = NULL;
    }
    free(matrix[i].data);
    matrix[i].data = NULL;
  }
  
  free(matrix);
  matrix = NULL;
  return 0;
}


bool validate_input_sw(char *input_ptr, size_t input_bytes, long timestep,
                    long point, long last_offset, long last_width) {
  #pragma HLS inline off
  if (point != -1) {
    if (last_offset <= point && point < last_offset + last_width) {
      long *input = reinterpret_cast<long *>(input_ptr);
      int size = input_bytes / sizeof(long);
      for (size_t i = 0; i < size; i++) {
#pragma HLS unroll
        if (i % 2 == 0) {
          if (!(input[i] == timestep - 1))
            return false;
        } else {
          if (!(input[i] == point))
            return false;
        }
      }
    }
  }
  return true;
}

// Kernel called by OMPC
void task_execute_sw(
    TaskGraph *graphs_array, // Contains all the information about the graphs
    char *in0_ptr,           // input buffer 0, if applicable
    char *in1_ptr,           // input buffer 1, if applicable
    char *in2_ptr,           // input buffer 2, if applicable
    char *in3_ptr,           // input buffer 3, if applicable
    char *in4_ptr,           // input buffer 4, if applicable
    char *in5_ptr,           // input buffer 5, if applicable
    char *in6_ptr,           // input buffer 6, if applicable
    char *in7_ptr,           // input buffer 7, if applicable
    char *in8_ptr,           // input buffer 8, if applicable
    char *output_ptr,        // output buffer
    bool *validated,         // validation flag
    int graph_idx,           // index of this graph
    int num_args,            // number of input buffers used
    long point,              // position x in the graph (point)
    long timestep            // position y in the graph (step)
) {
#pragma HLS INTERFACE m_axi depth=1024 port = graphs_array
#pragma HLS INTERFACE m_axi depth=1024 port = in0_ptr
#pragma HLS INTERFACE m_axi depth=1024 port = in1_ptr
#pragma HLS INTERFACE m_axi depth=1024 port = in2_ptr
#pragma HLS INTERFACE m_axi depth=1024 port = in3_ptr
#pragma HLS INTERFACE m_axi depth=1024 port = in4_ptr
#pragma HLS INTERFACE m_axi depth=1024 port = in5_ptr
#pragma HLS INTERFACE m_axi depth=1024 port = in6_ptr
#pragma HLS INTERFACE m_axi depth=1024 port = in7_ptr
#pragma HLS INTERFACE m_axi depth=1024 port = in8_ptr
#pragma HLS INTERFACE m_axi depth=1024 port = output_ptr
#pragma HLS INTERFACE m_axi depth=32 port = validated
#pragma HLS INTERFACE s_axilite port = graph_idx
#pragma HLS INTERFACE s_axilite port = num_args
#pragma HLS INTERFACE s_axilite port = point
#pragma HLS INTERFACE s_axilite port = timestep
#pragma HLS INTERFACE s_axilite port = return

  validated[0] = true;
  validated[1] = true;
  validated[2] = true;
  validated[3] = true;
  validated[4] = true;

  // Gets the graph object
  TaskGraph graph = graphs_array[graph_idx];

  // Generic graph validations
  if (!(0 <= timestep && timestep < graph.timesteps)) {
    validated[0] = false;
  }

  long offset = core::offset_at_timestep(graph.dependence, timestep,
                                         graph.max_width, graph.timesteps);
  long width = core::width_at_timestep(graph.dependence, timestep,
                                       graph.max_width, graph.timesteps);

  if (!(offset <= point && point < offset + width)) {
    validated[1] = false; 
  }

  // Gets an array specifying the dependency for each input
  long deps[MAX_INPUT_NUMBER] = {-1, -1, -1, -1, -1, -1, -1, -1, -1};
  core::dependencies(graph, timestep, point, deps);

  long last_offset = core::offset_at_timestep(graph.dependence, timestep - 1,
                                              graph.max_width, graph.timesteps);
  long last_width = core::width_at_timestep(graph.dependence, timestep - 1,
                                            graph.max_width, graph.timesteps);

  // Evaluate each input based on the dependencies we just got
  switch (num_args) {
  case 9:
    validated[2] = validate_input_sw(in8_ptr, graph.output_bytes_per_task, timestep,
                                deps[8], last_offset, last_width);
  case 8:
    validated[2] = validate_input_sw(in7_ptr, graph.output_bytes_per_task, timestep,
                                deps[7], last_offset, last_width);
  case 7:
    validated[2] = validate_input_sw(in6_ptr, graph.output_bytes_per_task, timestep,
                                deps[6], last_offset, last_width);
  case 6:
    validated[2] = validate_input_sw(in5_ptr, graph.output_bytes_per_task, timestep,
                                deps[5], last_offset, last_width);
  case 5:
    validated[2] = validate_input_sw(in4_ptr, graph.output_bytes_per_task, timestep,
                                deps[4], last_offset, last_width);
  case 4:
    validated[2] = validate_input_sw(in3_ptr, graph.output_bytes_per_task, timestep,
                                deps[3], last_offset, last_width);
  case 3:
    validated[2] = validate_input_sw(in2_ptr, graph.output_bytes_per_task, timestep,
                                deps[2], last_offset, last_width);
  case 2:
    validated[2] = validate_input_sw(in1_ptr, graph.output_bytes_per_task, timestep,
                                deps[1], last_offset, last_width);
  case 1: // This is in0
    validated[2] = validate_input_sw(in0_ptr, graph.output_bytes_per_task, timestep,
                                deps[0], last_offset, last_width);
  }

  // Validate and generate output
  if (!(graph.output_bytes_per_task >= (2 * sizeof(long)))) {
    validated[3] = false;
  }

  // Generate output
  long *output = reinterpret_cast<long *>(output_ptr);
  long size = graph.output_bytes_per_task / sizeof(long);
  for (size_t i = 0; i < size; i++) {
#pragma HLS unroll
    output[i] = (i % 2 == 0) ? timestep : point;
  }

  // Execute the kernel
  long iterations = graph.kernel.iterations;
  double imbalance = graph.kernel.imbalance;
 
  switch (graph.kernel.type) {
  case kernel_type_t::COMPUTE_BOUND: {
    double value = kernel::compute_bound(iterations);
    if (value < 128) {
      validated[4] = false;
    }
  } break;
  case kernel_type_t::COMPUTE_BOUND2: {
    double value = kernel::compute_bound_2(iterations);
    if (value < 128) {
      validated[4] = false;
    }
  } break;
  case kernel_type_t::LOAD_IMBALANCE: {
    double value =kernel::imbalance(iterations, imbalance, graph_idx, timestep, point);
    if (value <= 128) {
      validated[4] = false;
    }
  } break;
  case kernel_type_t::EMPTY:
  case kernel_type_t::MEMORY_BOUND:
  case kernel_type_t::COMPUTE_DGEMM:
  case kernel_type_t::MEMORY_DAXPY:
  case kernel_type_t::BUSY_WAIT:
  case kernel_type_t::IO_BOUND:
  default:
    kernel::empty();
    break;
  }
}


namespace core {
long offset_at_timestep(DependenceType dep, long timestep, long max_width,
                        long timesteps) {
  #pragma HLS inline off
  if (timestep < 0) {
    return 0;
  }

  switch (dep) {
  case DependenceType::TRIVIAL:
  case DependenceType::NO_COMM:
  case DependenceType::STENCIL_1D:
  case DependenceType::STENCIL_1D_PERIODIC:
    return 0;
  case DependenceType::DOM:
    return max(0L, timestep + max_width - timesteps);
  case DependenceType::TREE:
  case DependenceType::FFT:
  case DependenceType::ALL_TO_ALL:
  case DependenceType::NEAREST:
  case DependenceType::SPREAD:
  case DependenceType::RANDOM_NEAREST:
  case DependenceType::RANDOM_SPREAD:
    return 0;
  default:
    return 0;
  };
}

long width_at_timestep(DependenceType dep, long timestep, long max_width,
                       long timesteps) {
  #pragma HLS inline off
  if (timestep < 0) {
    return 0;
  }

  switch (dep) {
  case DependenceType::TRIVIAL:
  case DependenceType::NO_COMM:
  case DependenceType::STENCIL_1D:
  case DependenceType::STENCIL_1D_PERIODIC:
    return max_width;
  case DependenceType::DOM:
    return min(max_width, min(timestep + 1, timesteps - timestep));
  case DependenceType::TREE:
    return min(max_width, 1L << min(timestep, 62L));
  case DependenceType::FFT:
  case DependenceType::ALL_TO_ALL:
  case DependenceType::NEAREST:
  case DependenceType::SPREAD:
  case DependenceType::RANDOM_NEAREST:
  case DependenceType::RANDOM_SPREAD:
    return max_width;
  default:
    return 0;
  };
}

long max_dependence_sets(DependenceType dep, long max_width, long period) {
  #pragma HLS inline off
  switch (dep) {
  case DependenceType::TRIVIAL:
  case DependenceType::NO_COMM:
  case DependenceType::STENCIL_1D:
  case DependenceType::STENCIL_1D_PERIODIC:
  case DependenceType::DOM:
  case DependenceType::TREE:
    return 1;
  case DependenceType::FFT:
    return (long)ceil(log2(max_width));
  case DependenceType::ALL_TO_ALL:
  case DependenceType::NEAREST:
    return 1;
  case DependenceType::SPREAD:
  case DependenceType::RANDOM_NEAREST:
  case DependenceType::RANDOM_SPREAD:
    return period;
  default:
    return 0;
  };
}

long dependence_set_at_timestep(DependenceType dep, long timestep,
                                long max_width, long period) {
  #pragma HLS inline off
  long max_dep_set = max_dependence_sets(dep, max_width, period);

  switch (dep) {
  case DependenceType::TRIVIAL:
  case DependenceType::NO_COMM:
  case DependenceType::STENCIL_1D:
  case DependenceType::STENCIL_1D_PERIODIC:
  case DependenceType::DOM:
  case DependenceType::TREE:
    return 0;
  case DependenceType::FFT:
    return (timestep + max_dep_set - 1) % max_dep_set;
  case DependenceType::ALL_TO_ALL:
  case DependenceType::NEAREST:
    return 0;
  case DependenceType::SPREAD:
  case DependenceType::RANDOM_NEAREST:
  case DependenceType::RANDOM_SPREAD:
    return timestep % max_dep_set;
  default:
    return 0;
  };
}

size_t num_dependencies(DependenceType dep, long max_width, long radix) {
  #pragma HLS inline off
  size_t count = 0;

  switch (dep) {
  case DependenceType::TRIVIAL:
    return 0;
  case DependenceType::NO_COMM:
  case DependenceType::STENCIL_1D:
    return 1;
  case DependenceType::STENCIL_1D_PERIODIC:
    return max_width > 1 ? 2 : 3;
  case DependenceType::DOM:
  case DependenceType::TREE:
    return 1;
  case DependenceType::FFT:
    return 3;
  case DependenceType::ALL_TO_ALL:
    return 1;
  case DependenceType::NEAREST:
    return radix > 0 ? 1 : 0;
  case DependenceType::SPREAD:
  case DependenceType::RANDOM_NEAREST:
    return radix;
  default:
    break;
  };

  return SIZE_MAX;
}

void dependencies(TaskGraph graph, long timestep, long point,
                  long *final_deps) {
  #pragma HLS inline off
  long last_offset = core::offset_at_timestep(graph.dependence, timestep - 1,
                                              graph.max_width, graph.timesteps);
  long last_width = core::width_at_timestep(graph.dependence, timestep - 1,
                                            graph.max_width, graph.timesteps);
  long max_width = graph.max_width;
  long radix = graph.radix;

  long dset = core::dependence_set_at_timestep(graph.dependence, timestep,
                                               graph.max_width, graph.period);
  size_t max_deps =
      core::num_dependencies(graph.dependence, graph.max_width, graph.radix);

  std::pair<long, long> deps[3];
  int num_deps = 0;
  switch (graph.dependence) {
  case DependenceType::TRIVIAL: {
  } break;
  case DependenceType::NO_COMM: {
    deps[0] = std::pair<long, long>(point, point);
    num_deps = 1;
  } break;
  case DependenceType::STENCIL_1D: {
    deps[0] = std::pair<long, long>(max(0L, point - 1),
                                    min(point + 1, max_width - 1));
    num_deps = 1;
  } break;
  case DependenceType::STENCIL_1D_PERIODIC: {
    size_t idx = 0;
    deps[idx++] = std::pair<long, long>(max(0L, point - 1),
                                        min(point + 1, max_width - 1));
    if (point - 1 < 0) { // Wrap around negative case
      deps[idx++] = std::pair<long, long>(max_width - 1, max_width - 1);
    }
    if (point + 1 >= max_width) { // Wrap around positive case
      deps[idx++] = std::pair<long, long>(0, 0);
    }
    num_deps = idx;
  } break;
  case DependenceType::DOM: {
    deps[0] = std::pair<long, long>(max(0L, point - 1), point);
    num_deps = 1;

  } break;
  case DependenceType::TREE: {
    long parent = point / 2;
    deps[0] = std::pair<long, long>(parent, parent);
    num_deps = 1;
  } break;
  case DependenceType::FFT: {
    size_t idx = 0;
    long d1 = point - (1 << dset);
    long d2 = point + (1 << dset);
    if (d1 >= 0) {
      deps[idx++] = std::pair<long, long>(d1, d1);
    }
    deps[idx++] = std::pair<long, long>(point, point);
    if (d2 < max_width) {
      deps[idx++] = std::pair<long, long>(d2, d2);
    }
    num_deps = idx;
  } break;
  case DependenceType::ALL_TO_ALL: {
    deps[0] = std::pair<long, long>(0, max_width - 1);
    num_deps = 1;

  } break;
  case DependenceType::NEAREST: {
    if (radix > 0) {
      deps[0] =
          std::pair<long, long>(max(0L, point - radix / 2),
                                min(point + (radix - 1) / 2, max_width - 1));
      num_deps = 1;
    }
  } break;
  case DependenceType::SPREAD: {
    for (long i = 0; i < radix; ++i) {
      long dep =
          (point + i * max_width / radix + (i > 0 ? dset : 0)) % max_width;
      deps[i] = std::pair<long, long>(dep, dep);
    }
    num_deps = radix;
  } break;
  case DependenceType::RANDOM_NEAREST:
  default:
    break;
  };

  long offset = core::offset_at_timestep(graph.dependence, timestep,
                                         graph.max_width, graph.timesteps);
  long width = core::width_at_timestep(graph.dependence, timestep,
                                       graph.max_width, graph.timesteps);

  size_t idx = 0;
  for (size_t span = 0; span < num_deps; span++)
    for (long dep = deps[span].first; dep <= deps[span].second; dep++)
      final_deps[idx++] = dep;
}
} // namespace core

namespace kernel {
void empty() {
#pragma HLS inline off
  // Do nothing...
}

double compute_bound(long iterations) {
#pragma HLS inline off
  double A[64];

  for (int i = 0; i < 64; i++) {
    A[i] = 1.2345;
  }

  for (long iter = 0; iter < iterations; iter++) {
    for (int i = 0; i < 64; i++) {
      A[i] = A[i] * A[i] + A[i];
    }
  }

  double *C = (double *)A;
  double dot = 0.0;
  for (int i = 0; i < 64; i++) {
    dot += C[i];
  }

  return dot;
}

double compute_bound_2(long iterations) {
#pragma HLS inline off
  constexpr size_t N = 32;
  double A[N] = {0};
  double B[N] = {0};
  double C[N] = {0};

  for (size_t i = 0; i < N; ++i) {
    A[i] = 1.2345;
    B[i] = 1.010101;
  }

  for (long iter = 0; iter < iterations; iter++) {
    for (size_t i = 0; i < N; ++i) {
      C[i] = C[i] + (A[i] * B[i]);
    }
  }

  double sum = 0;
  for (size_t i = 0; i < N; ++i) {
    sum += C[i];
  }

  return sum;
}

double imbalance(long iterations, double imbalance, long graph_index,
               long timestep, long point) {
#pragma HLS inline off
  return compute_bound(iterations);
}
} // namespace kernel
